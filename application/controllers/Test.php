<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Test extends CI_Controller{

    function __construct(){
		parent::__construct();
	}
    public function index(){

    }

    public function lists($cateid) { 
        $cateid = (int) $cateid;
        $this->load->model('test_model','test');
        $this->load->model('category_model','category'); 
        // get cate detail
        $cate = $this->category->detail($cateid);
        $url = $this->uri->uri_string();
        if ($url != trim($cate['share_url'],'/')){
            redirect($cate['share_url'],'location','301');
        }
        $this->load->setData('seo_title',($cate['seo_title']) ? $cate['seo_title'] : $cate['name']);
        $this->load->setData('meta',array(
            'keyword' => ($cate['seo_keyword']) ? $cate['seo_keyword'] : $cate['name'],
            'description' => ($cate['seo_description']) ? $cate['seo_description'] : $cate['description']
        ));
        $this->load->setData('ogMeta',array(
                'og:image' => getimglink($cate['images']),
                'og:title' => ($cate['seo_title']) ? $cate['seo_title'] : $cate['name'],
                'og:description' => ($cate['seo_description']) ? $cate['seo_description'] : $cate['description'],
                'og:url' => current_url())
        );
        //Load list test
        $limit = 10;
        $this->load->model('test_model','test');
        $page = (int)$this->input->get('page');
        $page = ($page > 1) ? $page : 1;
        $offset = ($page - 1) * $limit;
        $params['limit'] = $limit; $params['offset'] = $offset;
        $params['time'] = ($times == 0) ? 0 : 1;
        $params['cate_id'] = $cateid;
        $config['total_rows'] = $this->test->get_total_by_cate($params);
        if ($config['total_rows'] < 1){
            return $this->load->layout('common/noresult');
        }
        $config['per_page'] = $limit;
        $this->load->library('pagination',$config);
        $data['paging'] =  $this->pagination->create_links();

        // get test by cate
        $params = array_merge(array('isclass' => 0), $params);

        $data['rows'] = $this->test->get_test_by_cate($params); 
        $data['cate'] = $cate;

        if($data['rows']){
            $this->load->config("data");
            $arr_test_type = $this->config->item('test_type');
            foreach ($data['rows'] as $key => $test) {
                foreach ($arr_test_type as $type_id => $type_name) {
                    // Check question
                    $arrQuestion = $this->test->get_question_by_test(array('test_id' => $test['test_id'], 'type' => $type_id, 'limit' => 200));
                    if($arrQuestion){
                        $data['rows'][$key]['test_list'][] = $type_name;
                    }
                }
            }
        }

        $breadcrumb = array(array('name' => $cate['name'],'link' => $cate['share_url']));
        $this->config->set_item("breadcrumb",$breadcrumb);
        /// set config
        $this->config->set_item("mod_access",array("name" => "test_list"));
        $this->config->set_item("menu_select",array('item_mod' => 'test_list', 'item_id' => $cateid,'item_type' => $cate['type']));
        // render view
        
        $this->load->layout('test/lists',$data);
    }
    public function class_home(){
        $classDetail = $this->permission->getClassIdentity();
        /// GET ARTICLE BY CLASS //////
        $this->load->model('news_model','news');
        $this->load->model('category_model','category');
        $arrNews = $this->news->list_for_class($classDetail['class_id'],array('limit' => 6));
        /// get all cate video
        $arrCategory = $this->category->get_category(array('type' => 1,'style' => 2));
        foreach ($arrCategory as $key => $cate) {
            $arrCateId[] = $cate['cate_id'];
        }

        $arrVideo = $this->news->list_for_class($classDetail['class_id'],array('limit' => 6, 'cate' => $arrCateId));

        $data = array(
            'arrNews' => $arrNews,
            'arrVideo' => $arrVideo,
            'classDetail' => $classDetail
        );
        $this->config->set_item("mod_access",array("name" => "class_test"));
        $this->config->set_item("menu_select",array('item_mod' => 'class_test'));
        $this->load->layout('test/class_home',$data);
    }
    public function class_list($type = 0,$times = 0){
        $classDetail = $this->permission->getClassIdentity();
        //print_r($this->session->userdata);
        $limit = 10; $cate_id = intval($cate_id);
        $this->load->model('test_model','test');
        $page = (int)$this->input->get('page');
        $page = ($page > 1) ? $page : 1;
        $offset = ($page - 1) * $limit;
        $option['limit'] = $limit; $option['offset'] = $offset;
        $option['time'] = ($times == 0) ? 0 : 1;
        $option['type'] = $type;
        $config['total_rows'] = $this->test->count_list_for_class($classDetail['class_id'],$option);
        if ($config['total_rows'] < 1){
            return $this->load->layout('common/noresult');
        }
        $config['per_page'] = $limit;
        $this->load->library('pagination',$config);
        $data['paging'] =  $this->pagination->create_links();
        $data['rows'] = $this->test->list_for_class($classDetail['class_id'],$option);
        $data['classDetail'] = $classDetail;

        $this->config->set_item("breadcrumb",array(array("name" => 'Bài tập test lớp',"link" => SITE_URL. '/bai-tap-lop.html')));

        $this->config->set_item("mod_access",array("name" => "class_test"));
        $this->config->set_item("menu_select",array('item_mod' => 'class_test'));

        $this->load->layout('test/class_list',$data);
    }
    public function detail($test_id,$type){
//        var_dump($_POST);exit;

        /////////// CHECK PERMISSION ////////
        $test_id = (int) $test_id;
        $this->load->config("data");
        $keyType = array_search ($type, array_map('strtolower', $this->config->item('test_type')));
        $this->load->model('test_model','test');
        $this->load->model('category_model','category');
        // get detail test
        $testDetail = $this->test->get_test_detail(array('test_id' => $test_id));
        ///////// get category //////////
        $cateDetail = $this->category->detail($testDetail['original_cate']);
        //Check user login
        if(!($user_id = $this->permission->getId())){
            $redirect_uri = SITE_URL.'/users/login?redirect_uri='.current_url();
            redirect($redirect_uri);
        }

        if (empty($testDetail)){
            show_404();
        }
        ///////// CHECK CLASS ////////
        $arrQuestion = $this->test->get_question_by_test(array('test_id' => $test_id,'type' => $keyType, 'limit' => 200));
        //var_dump($arrQuestion); die;
        if (empty($arrQuestion)){
            show_404();
        }

        foreach ($arrQuestion as $key => $questionDetail) {
            $arrQuestionId[] = $questionDetail['question_id'];
        }

        $arrQuestionGroup = $this->test->get_question_group(array('parent_id' => $arrQuestionId));
        $start_time = time();
        ///////// GET DETAIL
        $data = array('keyType' => $keyType,
            'test' => $testDetail,
            'cateDetail' => $cateDetail,
            'arrQuestion' => $arrQuestion,
            'arrQuestionGroup' => $arrQuestionGroup,
            'start_time' => $start_time,
            'token' => '123'
        );

        /////// Test type ///////
        $arr_list_test_type = array();
        $this->load->config("data");
        $arr_test_type = $this->config->item('test_type');
        foreach ($arr_test_type as $type_id => $type_name) {
            // Check question
            $arrQuestion = $this->test->get_question_by_test(array('test_id' => $test_id, 'type' => $type_id, 'limit' => 200));
            if($arrQuestion){
                $arr_list_test_type[] = $type_name;
            }
        }
        $data['arr_list_test_type'] = $arr_list_test_type;

        //////////////////////////////
        //////////////////////////////
        //////////////////////////////

        if (! $this->input->get('skill')) {
            // Lấy thêm thông tin bài test
            if ($this->input->post('fulltest_timestamp')) {
                // Timestamp Exist
                $fulltest_timestamp = $this->input->post('fulltest_timestamp');
                $fulltest_all_step = unserialize($this->input->post('fulltest_all_step'));
                $fulltest_now_step = (int) $this->input->post('fulltest_now_step');

                // Mặc định:
                $arr_fulltest_all_detail = array(
                    'fulltest_timestamp' => $fulltest_timestamp,
                    'fulltest_all_step' => serialize($fulltest_all_step),
                    'fulltest_now_step' => $fulltest_now_step,
                );
//                var_dump($arr_fulltest_all_detail);exit;

            } else {
//                // generate fulltest timestamp
                $fulltest_all_step = array();
//                var_dump($arr_list_test_type);
                foreach ($arr_list_test_type as $type_fcking) {
                    $fulltest_all_step[] = str_replace('/test/', '/test/' . trim(strtolower($type_fcking)) . '/', $testDetail['share_url']);
                }
//                var_dump($fulltest_all_step); exit();
                $arr_fulltest_all_detail = array(
                    'fulltest_timestamp' => time(),
                    'fulltest_all_step' => serialize($fulltest_all_step),
                    'fulltest_now_step' => 0,
                );
            }
        }
        if (isset($arr_fulltest_all_detail)) $data['arr_fulltest_all_detail'] = $arr_fulltest_all_detail;

        //////////////////////////////
        //////////////////////////////
        //////////////////////////////


        /////// SEO //////
        $this->load->setData('title',$testDetail['title'] .' -' .$type);
        $this->load->setData('meta',array(
            'keyword' => $testDetail['title'],
            'description' => cut_text($testDetail['description'],300)
        ));
        $this->load->setData('ogMeta',array(
                'og:image' => getimglink($testDetail['images']),
                'og:title' => $testDetail['title'],
                'og:description' => $testDetail['description'],
                'og:url' => current_url())
        );
        return $this->load->view('test/'.$type,$data,FALSE);
    }
    public function writing_result() {
        if ($this->input->post('fulltest_timestamp') ){
            // Full test
            $fulltest_timestamp = $this->input->post('fulltest_timestamp');
            $fulltest_all_step = unserialize($this->input->post('fulltest_all_step'));
            $fulltest_now_step = (int) $this->input->post('fulltest_now_step');

            // Mặc định:
            $arr_fulltest_all_detail = array(
                'fulltest_timestamp' => $fulltest_timestamp,
                'fulltest_all_step' => serialize($fulltest_all_step),
                'fulltest_now_step' => (int) $fulltest_now_step + 1,
            );
            if ($fulltest_now_step >= count($fulltest_all_step)){
                echo 'this is end of Full test';
                echo 'fulltest_timestamp: '.$fulltest_timestamp;

                exit();



            }else{
                $arr_fulltest_all_detail['url'] = $fulltest_all_step[$fulltest_now_step + 1];
                return $this->load->view('common/redirect_post',$arr_fulltest_all_detail,false);
            }
        }

        $this->load->model('test_model','test');
        $this->load->model('category_model','category');
        $test_id = (int) $this->input->post('test_id');
        $type = (int) $this->input->post('type');
        $start_time = $this->input->post('start_time');
        if(!($testDetail = $this->test->get_test_detail(array('test_id' => $test_id)))) {
            show_404();
        }
        $arrUserAnswer = $this->input->post('answer');
        if (!is_array($arrUserAnswer)) {
            $arrUserAnswer = @json_decode($arrUserAnswer,TRUE);
        }
        $cateDetail = $this->category->detail($testDetail['original_cate']);
        $arrParentQuestion = $this->test->get_question_by_test(array('test_id' => $test_id,'type' => $type, 'parent' => 0, 'limit' => 200));
        //var_dump($arrQuestion); die;
        if (empty($arrParentQuestion)){
            show_404();
        }
        $total_time = 0;
        foreach ($arrParentQuestion as $key => $questionDetail) {
            $arrParentId[] = $questionDetail['question_id'];
            $total_time += $questionDetail['test_time'];
        }
        $arrQuestion = $this->test->get_question_group(array('parent_id' => $arrParentId));
        $arrQuestion =  array_shift($arrQuestion);
        
        ///// GET ARRANSER 

        $data = array(
            'testDetail' => $testDetail,
            'total_question' => $total_question,
            'arrQuestion' => $arrQuestion,
            'replay_url' => replace_test_link($testDetail['share_url'],3)
        );

        //////// USSER //////
        $userData = $this->permission->getIdentity();

        /////////////// INSERT LOG ///////////
//        $score_converted = $this->test->get_score($answer_true);
//
//        $arr_res = $this->test->getResult_and_Suggest_byPoint($type,$score_converted);
//        $score_text = $arr_res[0];
//        $score_suggest = $arr_res[1];


        if ($this->input->post('fulltest_timestamp')) {
            $fulltest_timestamp_log =  (int) $this->input->post('fulltest_timestamp');
        }else{
            $fulltest_timestamp_log = 0;
        }
        $test_log_id = $this->test->test_logs_insert(array('test_id' => $test_id, 'timestamp_fulltest'=> $fulltest_timestamp_log,'user_id' => (int) $userData['user_id'], 'test_type' => $type, 'start_time' => $start_time,'end_itme' => time(), 'score' => '$score_converted','score_detail' => array('true' => '$answer_true','arrAnswerTrue' => '$arrAnswerTrue'),'answer_list' => '$arrUserAnswer'));




        //////////////////
        $this->config->set_item("breadcrumb",array(array('name' => $cateDetail['name'],'link' => $cateDetail['share_url']),array("name" => 'Kết quả test')));



        return $this->load->layout('test/result_writing',$data);
    }
    public function review_result($log_id, $result) {
        $this->load->model('test_model','test');
        $this->load->model('category_model','category');
        $this->load->config("data");
        ////// get log detail ////////
        $arrLogDetail = $this->test->get_test_logs_detail($log_id);
        //var_dump($arrLogDetail); die;
        /////////// CHECK PERMISSION ////////
        $test_id = (int) $arrLogDetail['test_id'];
        $keyType = $arrLogDetail['test_type'];

        $type = $this->config->item('test_type');
        $type = strtolower($type[$keyType]);
        // get detail test 
        $testDetail = $this->test->get_test_detail(array('test_id' => $test_id));
        ///////// get category //////////
        $cateDetail = $this->category->detail($testDetail['original_cate']);



        /* $userData = $this->permission->getIdentity();
        if ($testDetail['test_time'] > 0 && !$userData) {
            return redirect(SITE_URL.'/users/login?redirect_uri='.current_url(),'refresh');
        }*/
        if (empty($testDetail)){
            show_404();
        }
        ///////// CHECK CLASS ////////
        $arrQuestion = $this->test->get_question_by_test(array('test_id' => $test_id,'type' => $keyType, 'limit' => 200));
        //var_dump($arrQuestion); die;
        if (empty($arrQuestion)){
            show_404();
        }
        
        foreach ($arrQuestion as $key => $questionDetail) {
            $arrQuestionId[] = $questionDetail['question_id'];
        }
        $arrQuestionGroup = $this->test->get_question_group(array('parent_id' => $arrQuestionId));
        //
        $id_question_to_group_question = array();
        $id_answer_to_id_question = array();
        $arr_id_answer_to_group_question = array();
        foreach ($arrQuestionGroup as $id_group_question => $arr_question_inside) {
            foreach ($arr_question_inside as $id_question_inside => $item) {
                $id_question_to_group_question[$id_question_inside] = $id_group_question;
                $question_answer_arr = $item['question_answer'];
                for ($k = 0; $k < count($question_answer_arr); $k++) {
                    // [answer_id] => 44     [question_id] => 84
                    $answer_id_mono = $question_answer_arr[$k]['answer_id'];
                    $question_id = $question_answer_arr[$k]['question_id'];
                    $id_answer_to_id_question[$answer_id_mono] = $question_id;
                }
            }
        }

        foreach ($id_answer_to_id_question as $id_answer => $id_question) {
            $arr_id_answer_to_group_question[$id_answer] = $id_question_to_group_question[$id_question];
        }

        $arrQuestionId = array();
        foreach ($arrQuestionGroup as $key => $questionGroup) {
            $arrQuestionId = array_merge($arrQuestionId,array_keys($questionGroup));
        }

        $arrAnswer = $this->test->get_answer_result_by_question_id($arrQuestionId);
        foreach ($arrAnswer as $key => $answer) {
            $arrAnswerResult[$answer['answer_id']][] = $answer;
        }
        //var_dump($arrAnswerResult); die;
        $start_time = time();
        ///////// GET DETAIL 
        $data = array('keyType' => $keyType,
                        'test' => $testDetail,
                        'arrQuestion' => $arrQuestion,
                        'arrQuestionGroup' => $arrQuestionGroup,
                        'start_time' => $start_time,
                        'token' => '123',
                        'cateDetail' => $cateDetail,
                        'arrLogDetail' => $arrLogDetail,
                        'arrAnswerResult' => $arrAnswerResult,
                        'arr_id_answer_to_group_question' => $arr_id_answer_to_group_question,
        );


        /////// SEO //////
        $this->load->setData('title',$testDetail['title'] .' -' .$type);
        $this->load->setData('meta',array(
            'keyword' => $testDetail['title'],
            'description' => cut_text($testDetail['description'],300)
        ));
        $this->load->setData('ogMeta',array(
                'og:image' => getimglink($testDetail['images']),
                'og:title' => $testDetail['title'],
                'og:description' => $testDetail['description'],
                'og:url' => current_url())
        );
        return $this->load->view('test/'.$type.'_review',$data,FALSE);
    } 
    public function result() {
        $this->load->model('test_model','test');
        $this->load->model('category_model','cate');
        $test_id = (int) $this->input->post('test_id');
        $type = (int) $this->input->post('type');
        $start_time = $this->input->post('start_time');
        $this->load->config("data");
        //////// USSER //////
        $userData = $this->permission->getIdentity();
        // update count hit
        $this->test->incre_hit(array('test_id' => $test_id));
        
        if(!($testDetail = $this->test->get_test_detail(array('test_id' => $test_id)))) {
            var_dump($_POST);
            show_404();
        }

        //////////// GET TEST RELATE ///////
        $cateDetail = $this->cate->detail($testDetail['original_cate']);
        $arrTestRelate = $this->test->get_test_by_cate(array('excluse' => $test_id,'cate_id' => $testDetail['original_cate']));
        //////////////
        $arrUserAnswer = $this->input->post('answer');
        if (!is_array($arrUserAnswer)) {
            $arrUserAnswer = @json_decode($arrUserAnswer,TRUE);
        }

        //////////////////////////// GET Question //
        $arrParentQuestion = $this->test->get_question_by_test(array('test_id' => $test_id,'type' => $type, 'parent' => 0, 'limit' => 200));
        //var_dump($arrQuestion); die;
        if (empty($arrParentQuestion)) {
            show_404();
        }
        $total_time = 0;
        foreach ($arrParentQuestion as $key => $questionDetail) {
            $arrParentId[] = $questionDetail['question_id'];
            $total_time += $questionDetail['test_time'];
        }
        $arrQuestionId = $this->test->get_question_group(array('parent_id' => $arrParentId,'return_question_id_only' => 1));
        
        /// get answer question //
        $arrAnswer = $this->test->get_answer_result_by_question_id($arrQuestionId);
        $total_question = count($arrAnswer);
        foreach ($arrAnswer as $key => $answer) {
            $arrAnswerResult[$answer['answer_id']][] = $answer;
        }
        // check result
        $answer_true = 0; $arrAnswerTrue = array();
        foreach ($arrAnswerResult as $key1 => $arrAnswerLists) {
            $userAnswer = $arrUserAnswer[$key1];
            if (!$userAnswer || count($userAnswer) != count($arrAnswerLists)) {
                continue;
            }
            reset($userAnswer);
            foreach ($arrAnswerLists as $key2 => $value) {
                //var_dump($userAnswer[$key2],$value);
                if (strtolower(trim($value['answer'])) == strtolower(trim($userAnswer[$key2]))) {
                    $answer_true ++;
                    $arrAnswerTrue[$key1][$key2] = 1;
                }
            }
        }

        // What id save this ressult !!!!!!!!!!!!!!!!

        /////////////// INSERT LOG ///////////
        $score_converted = $this->test->get_score($answer_true);

        $arr_res = $this->test->getResult_and_Suggest_byPoint($type,$score_converted);
        $score_text = $arr_res[0];
        $score_suggest = $arr_res[1];


        if ($this->input->post('fulltest_timestamp')) {
            $fulltest_timestamp_log =  (int) $this->input->post('fulltest_timestamp');
        }else{
            $fulltest_timestamp_log = 0;
        }
        $test_log_id = $this->test->test_logs_insert(array('test_id' => $test_id, 'timestamp_fulltest'=> $fulltest_timestamp_log,'user_id' => (int) $userData['user_id'], 'test_type' => $type, 'start_time' => $start_time,'end_itme' => time(), 'score' => $score_converted,'score_detail' => array('true' => $answer_true,'arrAnswerTrue' => $arrAnswerTrue),'answer_list' => $arrUserAnswer));

        $spent_time = time() - $start_time;
        $spent_time_format =  sprintf('%02d:%02d', ($spent_time/60%60), $spent_time%60);
        $data = array(
            'arrTestRelate' => $arrTestRelate,
            'test_log_id' => $test_log_id,
            'type' => $type,
            'testDetail' => $testDetail,
            'score_converted' => $score_converted,
            'score_text' => $score_text,
            'score_suggest' => $score_suggest,
            'answer_true' => $answer_true,
            'total_question' => $total_question,
            'spent_time' => $spent_time_format,
            'arrAnswerTrue' => $arrAnswerTrue,
            'arrAnswerResult' => $arrAnswerResult,
            'arrUserAnswer' => $arrUserAnswer,
            'spent_time_percent' => round($spent_time / ($total_time * 60)*100),
            'replay_url' => replace_test_link($testDetail['share_url'],$type) . '?skill=1'
        );

        $this->config->set_item("breadcrumb",array(array('name' => $cateDetail['name'],'link' => $cateDetail['share_url']),array("name" => 'Kết quả test')));

        if ($this->input->post('fulltest_timestamp') ){
            // Full test
            $fulltest_timestamp = $this->input->post('fulltest_timestamp');
            $fulltest_all_step = unserialize($this->input->post('fulltest_all_step'));
            $fulltest_now_step = (int) $this->input->post('fulltest_now_step');

            // Mặc định:
            $arr_fulltest_all_detail = array(
                'fulltest_timestamp' => $fulltest_timestamp,
                'fulltest_all_step' => serialize($fulltest_all_step),
                'fulltest_now_step' => (int) $fulltest_now_step + 1,
            );
            if ($fulltest_now_step >= ( count($fulltest_all_step)-1 ) ){
                echo 'this is end';exit();
            }else{
                $arr_fulltest_all_detail['url'] = $fulltest_all_step[$fulltest_now_step + 1];
                return $this->load->view('common/redirect_post',$arr_fulltest_all_detail,false);
            }


        }
        return $this->load->layout('test/result_listread',$data);
    }


}