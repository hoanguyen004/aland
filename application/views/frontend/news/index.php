<!-- Trending now -->
<?php echo $this->load->get_block('trending'); ?>
<!-- End trending -->
<section class="container clearfix">
	<section class="container clearfix">
        <div class="row">
            <div id="top_banner" class="col-md-8 col-sm-8 col-xs-8 col-tn-12 mb20">
                <?php echo $this->load->get_block('home_banner'); ?>
            </div>
            <div id="sidebar_right" class="col-md-4 col-sm-4 col-xs-4 col-tn-12">
            	<div class="category resign_pc">
	                <?php echo DEVICE_ENV == 4 ? $this->load->view("block/contact") : ''; ?>
	            </div>
            </div>
        </div>
    </section>

    <!-- Khóa học trang chủ -->
    <?php echo $this->load->get_block('home_course'); ?>

    <!-- Box tư vấn mobile -->
    <div class="resign_mobile category">
        <?php echo DEVICE_ENV == 1 ? $this->load->view("block/contact") : ''; ?>
    </div>
    <!-- End box tư vấn mobile -->
    
	<div class="row">
		<div id="sidebar_left" class="col-md-8 col-sm-8 col-xs-8 col-tn-12">
			<?php echo $this->load->get_block('home_content'); ?>
		</div>
		<div id="sidebar_right" class="col-md-4 col-sm-4 col-xs-4 col-tn-12">
			<?php echo $this->load->get_block('home_right'); ?>
		</div>
	</div>
	
	<?php echo $this->load->get_block('home_center'); ?>
	<div class="row">
		<div id="sidebar_left" class="col-md-8 col-sm-8 col-xs-8 col-tn-12 mb20">
			<?php echo $this->load->get_block('left_content'); ?>
		</div>
		<div id="sidebar_right" class="col-md-4 col-sm-4 col-xs-4 col-tn-12">
			<?php echo $this->load->get_block('home_footer_right'); ?>
		</div>
	</div>
	<?php echo $this->load->get_block('home_footer_2'); ?>
	<div class="clearfix"></div>
	<?php if ($arrTags) {?>
	<div class="tag_hot mb20">
		<label>NỔI BẬT</label>
		<div class="warp">
			<?php foreach ($arrTags as $key => $tag) {?>
				<a href="<?php echo $tag['share_url']; ?>" title="<?php echo $tag['name']; ?>"><?php echo $tag['name']; ?></a>
			<?php }?>
		</div>
	</div>
	<?php }?>
	<?php echo $this->load->get_block('home_footer'); ?>
</section>