<?php
	$arrTestType = $this->config->item('test_type');
	$test_type = $arrTestType[$type];
?>
<section class="container clearfix m_height">
	<div class="row">
		<div id="sidebar_left" class="col-md-8 col-sm-8 col-xs-8 col-tn-12">
			<div class="warp_bg mb20">
				<?php echo $this->load->view('common/breadcrumb');?>
				<div class="infomation">
				  <img class="ava" src="images/graphics/thumb_5x4.jpg" alt="">
				  <div class="content">
					<h1><?php echo $testDetail['title']; ?> - <?php echo $test_type; ?></h1>
					<!--<p>Biên soạn bởi Aland English - Expert in IELTS</p>-->
					<div class="date">
					  	<p><i class="fa fa-file-text-o"></i>Số lần test: <?php echo $testDetail['total_hit']?></p>
					  	<?php if($testDetail['author']) { ?>
				  			<p><i class="fa fa-file-zip-o"></i>Người tạo: <?php echo $testDetail['author']?></p>
						<?php } ?>
					</div>                        
				  </div>                  
				</div>
				<div class="result-test writing border_top">
					<h2>Kết quả của bạn</h2>
					<div class="writing-result">
					  <div class="practice">
						  <img src="images/icons/pencil.png" alt="">  
						  <p>Gramaly check</p>
					  </div>
					  <div class="content">
						<div class="warp">
							<!-- Lỗi sai ngữ pháp -->
						  	<?php if($gramaly['grammar']) { ?>
						  		<p>Lỗi sai ngữ pháp : <strong>
							  		<?php foreach($gramaly['grammar'] as $item) { ?>
							  			<?php echo $item['bad'].((end($gramaly['grammar']) != $item) ? ';' : '')?>
							  		<?php } ?>
						  		</strong></p>
						  	<?php } ?>
						  	<!-- Lỗi sai từ vựng -->
						  	<?php if($gramaly['spelling']) { ?>
						  		<p>Lỗi sai từ vựng : <strong>
							  		<?php foreach($gramaly['spelling'] as $item) { ?>
							  			<?php echo $item['bad'].((end($gramaly['spelling']) != $item) ? ';' : '')?>
							  		<?php } ?>
						  		</strong></p>
						  	<?php } ?>
						  	<!-- Lỗi sai cấu trúc -->
						  	<?php if($gramaly['style']) { ?>
						  		<p>Lỗi sai cấu trúc : <strong>
							  		<?php foreach($gramaly['style'] as $item) { ?>
							  			<?php echo $item['bad'].((end($gramaly['style']) != $item) ? ';' : '')?>
							  		<?php } ?>
						  		</strong></p>
						  	<?php } ?>
						  	<textarea class="input_form input_area" onblur="if(this.value=='') this.value=this.defaultValue" onfocus="if(this.value==this.defaultValue) this.value=''" placeholder="Answer..."></textarea>
						</div>  
					  </div>
					</div>
					<div class="center">
						<!-- <a class="btn" href="/test/review_result/<?php echo $test_log_id; ?>/<?php echo $this->security->generate_token_post($test_log_id); ?>">Xem lại bài làm của bạn</a> -->
						<a class="btn" href="<?php echo $replay_url; ?>">Làm lại đề thi này</a>
						<a class="send" href="">
							<strong>Gửi yêu cầu chấm bài chi tiết</strong>
							<span>Giáo viên 8.5 IELTS tại Aland chấm</span>
						</a>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<?php foreach($arrQuestion as $key => $question) {
			$options = json_decode($question['question_answer'][0]['options'],true);
			$vocabulary = $question['question_answer'][0]['dict'];
			?>
			<div class="idea-test">
				<h2 class="title_center">Idea + vocabulary: <?php echo $question['title']; ?></h2>
				<div class="warp_bg mb20 threads">
					<p><strong>ĐỀ BÀI <?php echo $question['title']; ?></strong></p>
					<div class="de_bai">
						<?php echo $question['question_answer'][0]['content']; ?>
					</div>
				</div>
				<div class="row">
				  <div class="col-md-6">
					<div class="warp">
					  <h3><span>Idea</span></h3>
					  <div class="clearfix">
					  		<?php echo $options['idea']; ?>
					  </div>
					</div>
				  </div>                  
				  <div class="vocabulary col-md-6">
					<div class="warp">
					  	<h3><span>Vocabulary</span></h3>
					  	<?php if($vocabulary) { ?>
					  	<ul>
						  	<?php $i = 1;  foreach ($vocabulary as $item) { ?>
						  		<li>
									<a href="javascript:;" class="volume play" data-content="<?php echo $item['word_en']?>"><i class="fa fa-volume-up"></i></a>
									<div class="phien-am">
										<p><strong><?php echo $i.'. '.$item['word_en']?></strong></p>
										<span><?php echo $item['trans']?></span>
									</div>
									<div class="translate">
										<?php echo $item['word_vn']?>
									</div>
							  	</li>
						  	<?php $i++; } ?>                                           
					  	</ul>
					  	<?php } ?>
					</div>
					
				  </div>  
				  <div class="clearfix"></div>                          
				</div> 
				<div class="warp_bg mb20 threads">
					<h3 class="text-center">Sample <?php echo $question['title']; ?></h3>
					<?php echo $options['sample']; ?>
				</div>                                       
			</div>
			<?php } ?>         
		</div>

		<div id="sidebar_right" class="col-md-4 col-sm-4 col-xs-4 col-tn-12 mb20">
		  	<?php echo $this->load->get_block('right'); ?>
		</div>
	</div>    
	<!-- TIN QUAN TÂM --> 
	<?php if($arrTestRelate) { ?>
	<div class="warp_bg mb20 art_other grid4">
		<h2>Các bài test khác</h2>
		<div class="list_learn list_learn_other">
			<div class="row">
				<?php foreach ($arrTestRelate as $key => $testDetail) {?>
				  	<div class="col-md-3 col-sm-3 col-xs-3 col-tn-12 mb10">
						<div class="ava">
						  	<a href="<?php echo $testDetail['share_url']; ?>" title="<?php echo $testDetail['title']; ?>">
								<span class="thumb_img thumb_5x3"><img title="" src="<?php echo getimglink($testDetail['images'], 'size6'); ?>" alt=""></span>
						  	</a>
						</div>
						<div class="content">
						  	<h3><a href="#" title="">IELTS Fighter test1</a></h3>
						  	<p><?php echo $arrTestType[$testDetail['type']]?></p>
						</div>
				  	</div>
			  	<?php }?>
		  	</div>
		</div>                    
  	</div>    
  	<?php }?>    
	<?php echo $this->load->get_block('left_content'); ?> 
</section>

<script type="text/javascript">
	onload = function() {
    if ('speechSynthesis' in window) with(speechSynthesis) {
        var playEle = document.querySelector('.play');
        playEle.addEventListener('click', onClickPlay);
        function onClickPlay() {
            flag = true;
            utterance = new SpeechSynthesisUtterance($(this).data('content'));
            utterance.voice = getVoices()[0];
            speak(utterance);
        }
    }else { /* speech synthesis not supported */
        msg = document.createElement('h5');
        msg.textContent = "Detected no support for Speech Synthesis";
        msg.style.textAlign = 'center';
        msg.style.backgroundColor = 'red';
        msg.style.color = 'white';
        msg.style.marginTop = msg.style.marginBottom = 0;
        document.body.insertBefore(msg, document.querySelector('div'));
    }
}
</script>