<?php
$arrTestType = $this->config->item('test_type');
$test_type = $arrTestType[$type];
?>
<section class="container clearfix m_height">
	<div class="row">
		<div id="sidebar_left" class="col-md-8 col-sm-8 col-xs-8 col-tn-12">
			<div class="warp_bg mb20">
				<?php echo $this->load->view('common/breadcrumb'); ?>
				<div class="infomation">
				  <img class="ava" src="<?php echo getimglink($testDetail['images'],'size3');?>" alt="<?php echo $testDetail['title']; ?>">
				  <div class="content">
					<h1><?php echo $testDetail['title']; ?> - <?php echo $test_type; ?></h1>
					<!--<p>Biên soạn bởi Aland English - Expert in IELTS</p>-->
					<div class="date">
					  	<p><i class="fa fa-calendar"></i>Ngày đăng: <?php echo date('d/m/Y', $testDetail['publish_time']); ?></p>
					  	<p><i class="fa fa-file-text-o"></i>Số lần test: <?php echo $testDetail['total_hit']?></p>
					  	<?php if($testDetail['author']) { ?>
					  		<p><i class="fa fa-file-zip-o"></i>Người tạo: <?php echo $testDetail['author']?></p>
						<?php } ?>
					</div>
				  </div>
				  <!--<div class="practice">
					  <img src="<?php echo $this->config->item("img"); ?>icons/book.png" alt="">
					  <p><?php echo $testDetail['title']; ?></p>
				  </div>-->
				  <div class="clearfix"></div>
				</div>
				<div class="result-test border_top">
					<h2>Kết quả của bạn</h2>
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4 col-tn-12 mb20">
							<div class="c100 number p<?php echo round($answer_true / $total_question * 100); ?>">
								<span><?php echo $answer_true; ?></span>
								<div class="slice">
									<div class="bar"></div>
									<div class="fill"></div>
								</div>
							</div>
							<p>Số câu trả lời đúng</p>
							<a  href="#result-popup" class="open-popup-link"><i class="fa fa-pie-chart"></i>&nbsp;&nbsp;Chi tiết</a>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-4 col-tn-12 mb20">
							<div class="c100 point p<?php echo round($score_converted / 9 * 100); ?>">
								<span><?php echo $score_converted; ?></span>
								<div class="slice">
									<div class="bar"></div>
									<div class="fill"></div>
								</div>
							</div>
							<p>Điểm của bạn</p>
							<a href="<?php echo $this->config->item('score_table')?>"><i class="fa fa-table"></i>&nbsp;&nbsp;Bảng quy điểm</a>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-4 col-tn-12 mb20">
							<div class="c100 time p<?php echo $spent_time_percent; ?>">
								<span><?php echo $spent_time; ?></span>
								<div class="slice">
									<div class="bar"></div>
									<div class="fill"></div>
								</div>
							</div>
							<p>Thời gian làm bài</p>
						</div>
					</div>
					<div class="center">
						<a class="btn" href="/test/review_result/<?php echo $test_log_id; ?>/<?php echo $this->security->generate_token_post($test_log_id); ?>">Xem đáp án chi tiết</a>
						<a class="btn" href="<?php echo $replay_url; ?>">Làm lại đề thi này</a>
					</div>
				</div>
			</div>
			<div class="warp_bg mb20 result-noti">
				<div class="row">
					<div class="col-md-4 col-sm-4 col-xs-4 col-tn-12 mb20">
						<div class="c100 point p75">
							<span><?php echo $score_converted; ?></span>
							<div class="slice">
								<div class="bar"></div>
								<div class="fill"></div>
							</div>
						</div>
						<h4>Reading</h4>
					</div>
					<div class="col-md-8 col-sm-8 col-xs-8 col-tn-12 mb20">
					  <label><?php echo $score_text; ?> </label>
					  <p><?php echo $score_suggest; ?> </p>
					</div>
				</div>

			</div>
		</div>

		<div id="sidebar_right" class="col-md-4 col-sm-4 col-xs-4 col-tn-12 mb20">

		 		<?php echo $this->load->get_block('right'); ?>
		</div>
	</div>
	<!-- TIN QUAN TÂM -->
	<div class="warp_bg mb20 art_other grid4">
		 <h2>Các bài test khác</h2>

		 <div class="list_learn list_learn_other">
			<div class="row">
				<?php foreach ($arrTestRelate as $key => $testDetail) {?>
			  <div class="col-md-3 col-sm-3 col-xs-3 col-tn-12 mb10">
				<div class="ava">
				  <a href="<?php echo $testDetail['share_url']; ?>" title="<?php echo $testDetail['title']; ?>">
					<span class="thumb_img thumb_5x3"><img title="" src="<?php echo getimglink($testDetail['images'], 'size6'); ?>" alt=""></span>
				  </a>
				</div>
				<div class="content">
				  <h3><a href="#" title="">IELTS Fighter test1</a></h3>
				  <p>Reading</p>
				</div>
			  </div>
			  <?php }?>

		  </div>
		</div>
	  </div>
	<?php echo $this->load->get_block('left_content'); ?>
</section>
<div id="result-popup" class="white-popup mfp-hide">
  <div class="content_poup width_common">
      <h2>Số câu trả lời đúng</h2>
      <div class="warp">
          <div class="scrollbar-inner">
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6 col-tn-12 mb20">
                  <div class="c100 number p<?php echo round($answer_true / $total_question * 100); ?>">
                    <span><?php echo $answer_true; ?>/<?php echo $total_question; ?></span>
                    <div class="slice">
                      <div class="bar"></div>
                      <div class="fill"></div>
                    </div>
                  </div>
                  <strong>Số câu trả lời đúng</strong>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 col-tn-12 mb20">
                  <div class="c100 point p<?php echo round($score_converted / 9 * 100); ?>">
                    <span><?php echo $score_converted?></span>
                    <div class="slice">
                      <div class="bar"></div>
                      <div class="fill"></div>
                    </div>
                  </div>
                  <strong>Điểm của bạn</strong>
                </div>
              </div>
              <a href="<?php echo $this->config->item('score_table')?>"><i class="fa fa-table"></i>&nbsp;&nbsp;Bảng quy điểm</a>
              <div class="list-result">


        		<?php
$number_question = 1;
$number_answer = 1;
$number_break = round($total_question / 2);

?>
                  <div class="left result">
                      <h4>Câu trả lời của bạn</h4>
                      <ul>
                      	<li>
                      <?php
foreach ($arrAnswerResult as $key1 => $arrAnswerLists) {
	$userAnswer = $arrUserAnswer[$key1];
	$check_count = (!$userAnswer || count($userAnswer) != count($arrAnswerLists)) ? FALSE : TRUE;
	if ($check_count == TRUE) {
		reset($userAnswer);
	}
	foreach ($arrAnswerLists as $key2 => $value) {

		?>

                              <p class="<?php echo ($check_count == TRUE AND strtolower(trim($value['answer'])) == strtolower(trim($userAnswer[$key2]))) ? 'true' : 'false'; ?> "><span><?php echo $number_question; ?>. <?php echo $userAnswer[$key2]; ?></span></p>

                        <?php
if ($number_question == $number_break) {
			echo '</li><li>';
		}

		$number_question++;}}?>
                        </li>
                      </ul>
                  </div>
                  <div class="right result">
                      <h4>Đáp án</h4>
                      <ul>
                      	<li>
                      	<?php
foreach ($arrAnswerResult as $key1 => $arrAnswerLists) {
	$userAnswer = $arrUserAnswer[$key1];
	if ($userAnswer) {
		reset($userAnswer);
	}

	foreach ($arrAnswerLists as $key2 => $value) {
		?>

                              <p><strong><?php echo $number_answer; ?>. <?php echo $value['answer']; ?></strong></p>


                        <?php
if ($number_answer == $number_break) {
			echo '</li><li>';
		}

		$number_answer++;}}?>
                        </li>
                      </ul>
                  </div>

              </div>
          </div>
      </div>
  </div>
</div>
<!--END POPUP-->
<script src="<?php echo $this->config->item("js"); ?>jquery.scrollbar.min.js"></script>
<script src="<?php echo $this->config->item("js"); ?>jquery.magnific-popup.min.js"></script>
<script>
  $(document).ready(function () {
    jQuery('.scrollbar-inner').scrollbar();
    $('.open-popup-link').magnificPopup({
      type:'inline',
      midClick: true,
      mainClass: 'mfp-with-zoom',
      fixedContentPos: false,
      fixedBgPos: true,
      overflowY: 'auto',
      closeBtnInside: true,
      preloader: false,
      removalDelay: 300,
    });
  });
</script>