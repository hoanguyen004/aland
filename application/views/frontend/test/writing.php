<?php
$question = $arrQuestion[0];



?>
<!DOCTYPE html>
<html lang="vi" xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=yes">
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="apple-mobile-web-app-title" content="<?php echo $seo_keyword?>" />
  <?php if ($meta) { 
    foreach ($meta as $key => $value) { ?>
      <meta name="<?php echo $key; ?>" content="<?php echo $value; ?>" />
    <?php }
  } ?>
  <?php if ($ogMeta) { 
    foreach ($ogMeta as $key => $value) { ?>
      <meta property="<?php echo $key; ?>" content="<?php echo $value; ?>" />
    <?php }
  } ?>
  <meta name="keywords" content="<?php echo $seo_keyword?>"> 
  <meta name="description" content="<?php echo $seo_description?>">
  <link rel="canonical" href="<?php echo BASE_URL?>"/>
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->config->item("img"); ?>favicon.ico">
  <title><?php echo $seo_title; ?></title>
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;subset=vietnamese" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lato:400,900" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo $this->config->item("css"); ?>fontAwesome/css/font-awesome.min.css" media="all" /> 
  <link rel="stylesheet" href="<?php echo $this->config->item("css"); ?>bootstrap.min.css" media="all" /> 
  <link rel="stylesheet" href="<?php echo $this->config->item("css"); ?>style.css" media="all" />  
  <link rel="stylesheet" href="<?php echo $this->config->item("css"); ?>customize.css" media="all" /> 
  <script type="text/javascript">
    SITE_URL = '<?php echo SITE_URL; ?>';
  </script>
  <script src="<?php echo $this->config->item("js"); ?>jquery-2.1.4.min.js"></script>
  <?php echo $this->config->item("tracking_code_header"); ?>
    <style type="text/css">
        /* Only in showing */
        .tilte_explain_question{
            display: none !important;
        }
        .content_explain_question{
            background-color: transparent !important;
            padding: 0 !important;
        }
    </style>

</head>

<body class="body_home listening-test vertical">
    <!--HEADER-->
    <header id="header" class="section header">
        <a class="img_logo" href="">
            <img class="logo_web" src="<?php echo $this->config->item("img"); ?>graphics/logo.png" alt="Trang chủ" data-was-processed="true">
        </a>
        <div class="btn_control_menu"><i class="fa fa-bars"></i></div>
        <div class="my_user">
            <a class="notification" href=""><i class="fa fa-globe"></i><span>2</span></a>
            <a class="login" href=""><i class="fa fa-user"></i></a>
            <a class="ava" href=""><img src="<?php echo $this->config->item("img"); ?>graphics/ava.png" alt="this"></a>
        </div> 
    </header>
    <!--END HEADER-->
    <section class="listening-test_head clearfix">
      <div class="container">
        <a class="img_logo" href="">
            <img class="logo_web" src="<?php echo $this->config->item("img"); ?>graphics/logo.png" alt="Trang chủ" data-was-processed="true">
        </a>
        <div class="select_sever">
            <?php 
            $i = 1; $time = 0;
            foreach ($arrQuestionGroup[$question['question_id']] as $key => $qgroup) {?>
              <a data-section="<?php echo $qgroup['question_id']; ?>" id="question_setion_selection_<?php echo $qgroup['question_id']; ?>" class="reading_change_section <?php if ($i == 1) echo 'active'; ?>" href="javascript:void(0)"><?php echo $qgroup['title']; ?></a>
            <?php $i ++; 
            } 
            $test_time = $question['test_time'] * 60 * 1000;
            ?>
        </div> 
        <div class="option_listening">
            <div class="timer"><i class="fa fa-clock-o"></i><span id="show_count_down"></span></div>
            <a class="nop_bai" href="javascript:voi(0)" id="submit_answer_result">Nộp bài</a>
            <select id="show_test">
                <option>Show Test Info</option>
                <option>Show Test Info</option>
            </select>            
        </div>        
      </div>
    </section>
   

    <section class="listening-test_container writing-test clearfix">
        <?php 
          echo $this->load->view('test/common/skill_menu_mobile',array('test' => $test,'type' => 'writing')); ?>    
        <div class="container warp_slidebar">
            <?php echo $this->load->view('test/common/skill_menu',array('test' => $test,'type' => 'writing')); ?>
            <div class="slidebar_2">
              <form class="form" method="POST" action="/test/writing_result" id="test_form">
                <input type="hidden" name="test_id" value="<?php echo $test['test_id']; ?>">
            <input type="hidden" name="type" value="<?php echo $keyType; ?>">
            <input type="hidden" name="start_time" value="<?php echo $start_time; ?>">
            <input type="hidden" name="token" value="<?php echo $token; ?>">
          <?php
          if (isset($arr_fulltest_all_detail)){
              echo '<input type="hidden" name="fulltest_timestamp" value="' . $arr_fulltest_all_detail['fulltest_timestamp'].  '">';
              echo '<input type="hidden" name="fulltest_all_step" value=\''. $arr_fulltest_all_detail['fulltest_all_step'] .'\'>';
              echo '<input type="hidden" name="fulltest_now_step" value="'.$arr_fulltest_all_detail['fulltest_now_step'].'">';
          }
          ?>

              <?php 
              $number_question = 1;
              foreach ($arrQuestionGroup[$question['question_id']] as $key => $qgroup) {
              switch ($number_question) {
                case 1:
                  $max_length_text= 200;
                  break;
                default:
                  $max_length_text= 300;
                  break;
              }
              ?>        
              <div class="warp_content question_section_content" id="question_section_<?php echo $qgroup['question_id']; ?>" <?php if ($number_question != 1) echo 'style="display: none"'; ?>>
                  <div class="huong_dan">
                    <a class="title" href="javascript:;" data-toggle="collapse" data-target="#huong_dan_<?php echo $qgroup['question_id']; ?>"><i class="fa fa-exclamation-circle"></i>Hướng dẫn làm bài</a>
                    <div id="huong_dan_<?php echo $qgroup['question_id']; ?>" class="collapse">
                      <div class="warp" style="padding: 10px;">
                        <?php echo $qgroup['detail']; ?> 
                      </div>
                    </div>
                  </div>
                  <h2><?php echo $qgroup['title']; ?> </h2>
                  <div class="writing_document max_width">
                      <?php echo $qgroup['question_answer'][0]['content']; ?>
                  </div>
                  <div class="writing_note">
                      <header>
                          <div>Bài làm của bạn : <strong><span id="test_writing_count_word_<?php echo $qgroup['question_id']; ?>" style="color:#0a77ec">0</span>/<?php echo $max_length_text; ?></strong> words</div>
                      </header>
                      <textarea data-max-word="<?php echo $max_length_text; ?>" data-question-id="<?php echo $qgroup['question_id']; ?>"  class="input_form input_area test_writing_input" name="user_answer" placeholder="Answer..."></textarea>
                  </div>
                  <?php if ($nextSection = $arrQuestionGroup[$question['question_id']][$key + 1]) { ?>
                  <h2><a class="reading_change_section" href="javascript:void(0)" data-section="<?php echo $nextSection['question_id']; ?>"><?php echo $arrQuestionGroup[$question['question_id']][$key + 1]['title']; ?>&nbsp;&nbsp;<i class="fa fa-chevron-circle-right"></i></a></h2>
                  <?php } ?>                
              </div>
              <?php $number_question ++;} ?>
              </form>
            </div>
        </div> 
    </section>    

    <div class="mask-content"></div>

    <!--MAIN MENU-->
    <nav id="main_menu" class="main_menu">   
       <div class="header_menu section">
           <span id="auto_close_left_menu_button" class="close_main_menu">×</span>   
           <div class="my_contact">
            <p><i class="fa fa-phone"></i><a style="color:#ff3333;font-weight:bold">0987 915 448</a></p>
            <p class="email"><a><i class="fa fa-envelope-o"></i>support@aland.edu.vn</a></p>
          </div>                  
       </div>
      <div class="block_scoll_menu section">
         <div class="block_search section">
            <form id="search" action="http://timkiem.MSN.net/" method="get">
               <div class="warp">
                  <input id="auto_search_textbox" maxlength="80" name="q" class="input_form" placeholder="Tìm kiếm" type="search">
                  <button type="submit" id="btn_search_ns" class="btn_search"><i class="fa fa-search"></i></button>
                  <button type="reset" class="btn_reset">×</button>
               </div>
            </form>
         </div>
         <div class="list_menu section" id="auto_footer_menu">
            <ul class="list_item_panel section" id="auto_footer_first_list">
               <li>
                  <a href="https://www.google.com/" class="link_item_panel">About us</a>
                  <span class="sub-icon">+</span>
                  <ul class="level2">
                     <li>
                        <a href="">About us 1</a>
                        <span class="sub-icon2">+</span> 
                        <ul class="level3">
                           <li>
                              <a href="">About us 2</a>
                           </li>
                           <li>
                              <a href="">About us 2</a>
                           </li>
                        </ul>
                     </li>
                     <li>
                        <a href="">About us 1</a>
                     </li>
                     <li>
                        <a href="">About us 1</a>
                     </li>
                  </ul>
               </li>              
               <li><a href="#" class="link_item_panel">IELTS Online</a></li>
               <li><a href="#" class="link_item_panel">Lịch khai giảng</a></li>
               <li><a href="#" class="link_item_panel">IELTS Test</a></li>
               <li><a href="#" class="link_item_panel">Lộ trình học</a></li>
               <li><a href="#" class="link_item_panel">Tuyển dụng</a></li>             
            </ul>
         </div>            
         <div class="footer_bottom">         
            <div class="bold_text_top width_common"><h3>Học tiếng anh trực tuyến<br>© 2018 aland.net</h3>
                <p>Trực thuộc công ty cổ phần giáo dục và đào tạo Imap Việt Nam</p>
                <div class="social">
                  <a href=""><i class="fa fa-facebook"></i></a>
                  <a href=""><i class="fa fa-google-plus"></i></a>
                  <a href=""><i class="fa fa-youtube-play"></i></a>
                </div>                
            </div>
        </div>                              
      </div>
    </nav>
    <!--END MAIN MENU-->     
    <a href="javascript:;" id="to_top"><i class="fa fa-long-arrow-up"></i></a>
    <script src="<?php echo $this->config->item("js"); ?>jquery.countdown.min.js"></script>
    <script src="<?php echo $this->config->item("js"); ?>jquery.countdown.min.js"></script>
    <script src="<?php echo $this->config->item("js"); ?>bootstrap.min.js"></script>
    <script src="<?php echo $this->config->item("js"); ?>jquery.scrollbar.min.js"></script>
<script type="text/javascript">
      $(function(){
        jQuery('.scrollbar-inner').scrollbar();
        $(".reading_change_section").bind("click",function(){
            var section_id = $(this).attr("data-section");
            $(".question_section_content").hide();
            $("#question_section_" + section_id).show();

            $(".reading_change_section").removeClass("active");
            $("#question_setion_selection_" + section_id).addClass("active");
        });

        $("#test_form").find("select").bind("change",function(){
            var number = $(this).attr("data-question-number");
        });

        $(".answer_recheck").bind("click",function(){

        })
        window.onbeforeunload = function() {
            //return "Data will be lost if you leave the page, are you sure?";
        };
        // scroll box ket qua
        //$('.scrollbar-inner').scrollbar();
        // count downtime
        function liftOff() {
            $("#test_form").submit();
        }

        var fiveSeconds = new Date().getTime() + parseInt(<?php echo (int) $test_time; ?>);
        $('#show_count_down').countdown(fiveSeconds, {elapse: true})
        .on('update.countdown', function(event) {
            var this_countdown = $('#show_count_down');
            if (event.elapsed) {
                // $this.html('Hết thời gian làm bài');
                return liftOff();
            } else {
                this_countdown.html(event.strftime('%H : %M : %S'));
            }
        });
        //////////// COUNT WORD ////////
        $( ".test_writing_input" ).keyup(function() {
            var regex = /\s+/gi;
            var wordcount = jQuery.trim($(this).val()).replace(regex, ' ').split(' ').length;
            var qgid = $(this).attr("data-question-id");
            var max_word = parseInt($(this).attr("data-max-word"));
            if (wordcount > max_word) {
                $("#test_writing_count_word_" + qgid).css('color','#f92c53');
            }
            else {
                $("#test_writing_count_word_" + qgid).css('color','');
            }
            $("#test_writing_count_word_" + qgid).text(wordcount);
        });

        /// submit 
        $("#submit_answer_result").bind("click",function(){
          var r = confirm("Bạn có chắc muốn nộp bài ?");
          if (r == true) {
            $("#test_form").submit();
          }
          return false;
          
        })
      });  


    </script>
</body>
</html>
