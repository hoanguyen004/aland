<?php 
  $params = json_decode($course['params'], true);
?>
<!--MENU MANIN PC-->
<nav id="main_menu_pc" class=""> 
    <div class="container">
        <ul>
            <li class="home"><a href="#" title="this"><i class="fa fa-home"></i></a></li>
            <li class="active"><a href="#" title="this">Thông tin học viên</a></li>
            <li><a href="#" title="this">Lộ trình học</a></li>
            <li><a href="#" title="this">Danh sách lịch học</a></li>
            <li><a href="#" title="this">Tải tài liệu</a></li>
            <li><a href="#" title="this">Video cho bạn</a></li>
            <li class="sub_khoahoc">
              <a href="#" title="this">Khóa học của tôi</a>
              <span class="sub-icon"><i class="fa fa-caret-down"></i></span>
              <ul class="level2">
                <li>
                  <article class="art_item">
                    <div class="thumb_art">
                      <a href="#" title="">
                        <span class="thumb_img thumb_5x3"><img title="" src="<?php echo $this->config->item('img')?>/graphics/thumb_5x3.jpg" alt=""></span>
                      </a>      
                    </div>
                    <div class="content">
                      <h3 class="title_news">
                        <a href="" title="this">Chào Tân Sinh viên giảm 1triệu học phí</a>
                      </h3>
                      <a class="tag done" href=""><i class="fa fa-check-circle"></i>&nbsp;&nbsp;Đã học</a>
                    </div>
                  </article>
                </li>
                <li>
                  <article class="art_item">
                    <div class="thumb_art">
                      <a href="#" title="">
                        <span class="thumb_img thumb_5x3"><img title="" src="<?php echo $this->config->item('img')?>/graphics/thumb_5x3.jpg" alt=""></span>
                      </a>      
                    </div>
                    <div class="content">
                      <h3 class="title_news">
                        <a href="" title="this">Chào Tân Sinh viên giảm 1triệu học phí</a>
                      </h3>
                      <a class="tag done" href=""><i class="fa fa-check-circle"></i>&nbsp;&nbsp;Đã học</a>
                    </div>
                  </article>
                </li>
                <li>
                  <article class="art_item">
                    <div class="thumb_art">
                      <a href="#" title="">
                        <span class="thumb_img thumb_5x3"><img title="" src="<?php echo $this->config->item('img')?>/graphics/thumb_5x3.jpg" alt=""></span>
                      </a>      
                    </div>
                    <div class="content">
                      <h3 class="title_news">
                        <a href="" title="this">Chào Tân Sinh viên giảm 1triệu học phí</a>
                      </h3>
                      <a class="tag play" href=""><i class="fa fa-play-circle"></i>&nbsp;&nbsp;Đang học</a>
                    </div>
                  </article>
                </li>
                <li>
                  <article class="art_item">
                    <div class="thumb_art">
                      <a href="#" title="">
                        <span class="thumb_img thumb_5x3"><img title="" src="<?php echo $this->config->item('img')?>/graphics/thumb_5x3.jpg" alt=""></span>
                      </a>      
                    </div>
                    <div class="content">
                      <h3 class="title_news">
                        <a href="" title="this">Chào Tân Sinh viên giảm 1triệu học phí</a>
                      </h3>
                      <a class="tag study" href=""><img title="" src="<?php echo $this->config->item('img')?>/icons/star.png" alt="">&nbsp;&nbsp;Sắp học</a>
                    </div>
                  </article>
                </li>                                        
              </ul>
            </li>
        </ul>
    </div>
</nav>
<!--MENU MANIN PC-->
<section class="container clearfix main-hv">
    <div class="info warp_bg mb20">
        <h2>Chào, <?php echo $row['fullname']?> - học viên lớp <?php echo $course['title']?>!</h2>
        <ul>
            <label class="title-label">Thông tin lớp</label>
            <li>
              <img src="<?php echo $this->config->item('img')?>/icons/icon-user.png" alt="this">
              <p>Họ và tên: <strong><?php echo $row['fullname']?></strong></p>                                  
            </li>
            <li>
              <img src="<?php echo $this->config->item('img')?>/icons/icon-class.png" alt="this">
              <p>Tên lớp: <strong><?php echo $course['title']?></strong></p>
            </li>
            <?php if($params['code']) { ?>
            <li>
              <img src="<?php echo $this->config->item('img')?>/icons/icon-class.png" alt="this">
              <p>Mã lớp: <strong><?php echo $params['code']?></strong></p>
            </li>
            <?php } ?>
            <?php if($course['teacher_name']) { ?>
            <li>
              <img src="<?php echo $this->config->item('img')?>/icons/icon-teacher.png" alt="this">
              <p>Giáo viên: <strong><?php echo $course['teacher_name']?></strong></p>
            </li>
            <?php } ?>
            <?php if($params['input']) { ?>
            <li>
              <img src="<?php echo $this->config->item('img')?>/icons/icon-file.png" alt="this">
              <p>Đầu vào: <strong><?php echo $params['input']?></strong></p>
            </li>
            <?php } ?>
            <?php if($params['output']) { ?>
            <li>
              <img src="<?php echo $this->config->item('img')?>/icons/icon-file2.png" alt="this">
              <p>Cam kết đầu ra: <strong><?php echo $params['output']?></strong></p>
            </li>
            <?php } ?>
            <?php if($params['start_date']) { ?>
            <li>
              <img src="<?php echo $this->config->item('img')?>/icons/icon-calendar.png" alt="this">
              <p>Ngày khai giảng: <strong><?php echo $params['start_date']?></strong></p>
            </li>
            <?php } ?>
            <?php if($params['end_date']) { ?>
            <li>
              <img src="<?php echo $this->config->item('img')?>/icons/icon-calendar.png" alt="this">
              <p>Ngày kết thúc(dự kiến) <strong><?php echo $params['end_date']?></strong></p>
            </li>
            <?php } ?>
            <?php if($params['support']) { ?>
            <li>
              <img src="<?php echo $this->config->item('img')?>/icons/icon-support.png" alt="this">
              <p>Tư vấn support <strong><?php echo $params['support']?></strong></p>
            </li>    
            <?php } ?>                            
        </ul>
        <a class="togle" href="javascript:;">Thu gọn &nbsp;<i class="fa fa-angle-double-up"></i></a>
    </div>
    <div class="row">
        <div id="sidebar_left" class="col-md-8 col-sm-8 col-xs-8 col-tn-12">
          <div class="warp_bg">
            <label class="title-label">Lộ trình học</label>
            <img width="100%" src="<?php echo $this->config->item('img')?>/graphics/lotrinh.png" alt="this">
          </div>
          <div class="clearfix"></div>
          <div class="note-lt">
             * Chú thích:
             <a href="javascript:;" class="ping1"><span></span>Đã học</a>  
             <a href="javascript:;" class="ping2"><span></span>Chưa học</a>  
             <a href="javascript:;" class="ping3"><span></span>Cần học gấp</a>  
          </div>

        </div>
        <div id="sidebar_right" class="col-md-4 col-sm-4 col-xs-4 col-tn-12 mb20">
          <div class="warp_bg thong_ke">
            <label class="title-label">Thống kê chung</label>
            <div class="info">
                <h3>Tổng số bài học</h3>
                <p class="true"><img src="<?php echo $this->config->item('img')?>/icons/true.png" alt="this">&nbsp;Đã học: <strong>10 bài</strong></p>
                <p class="false"><img src="<?php echo $this->config->item('img')?>/icons/false.png" alt="this">&nbsp;Chưa học: <strong>20 bài</strong></p>
            </div>
            <div class="chart"><img src="<?php echo $this->config->item('img')?>/graphics/bieu-do.png" alt="this"></div>
            <div class="result">
              <p>Số câu trả lời đúng: <strong><b style="color:#02cc9c">89</b>/200</strong></p>
              <div class="pt">
                  <span></span>
              </div>
              <span class="kq">72%</span>
              <div><a href="">Chi tiết</a></div>
            </div>
          </div>
        </div>
    </div>
    <div class="row">
        <div id="sidebar_left" class="col-md-8 col-sm-8 col-xs-8 col-tn-12">
            <!-- BẢNG LỊCH HỌC -->
            <div class="table-lichhoc table-responsive">          
              <table class="table">
                <thead>
                  <tr>
                    <th class="notif" colspan="5">
                      <img src="<?php echo $this->config->item('img')?>/icons/ping.png" alt="this">
                      Hôm nay , ngày <?php echo date('d/m/Y')?> bài học tiếp theo của bạn là:
                    </th>
                  </tr>
                  <tr>
                    <th class="th1" colspan="5"><strong>Ngữ pháp và từ vựng</strong></th>
                  </tr>                                           
                </thead>
                <tbody>
                  <tr>
                    <td>09/09/2019</td>
                    <td>18h - 20h</td>
                    <td class="center"><a class="btn" href="">Nội dung bài học</a></td>
                    <td class="center"><a class="btn" href="">Bài tập</a></td>
                    <td style="width:350px">
                      <a class="ping pg3" href="">Test1</a>
                      <a class="ping pg3" href="">Test2</a>
                      <a class="ping pg3" href="">Test3</a>
                      <a class="ping pg3" href="">Test4</a>
                    </td>
                  </tr>
                  <tr>
                    <td class="txt_title" colspan="5">Danh sách lịch học</td>
                  </tr>
                  <tr class="txt_min gray">
                    <td colspan="5"><span class="ping pg1">Buổi 1: Ngữ pháp và từ vựng</span></td>
                  </tr>                      
                  <tr class="gray">
                    <td>09/09/2019</td>
                    <td>18h - 20h</td>
                    <td class="center"><a class="btn" href="">Nội dung bài học</a></td>
                    <td class="center"><a class="btn" href="">Bài tập</a></td>
                    <td style="width:350px">
                      <a class="ping pg1" href="">Test1</a>
                      <a class="ping pg2" href="">Test2</a>
                      <a class="ping pg3" href="">Test3</a>
                      <a class="ping pg3" href="">Test4</a>
                    </td>
                  </tr>
                  <tr class="txt_min">
                    <td colspan="5"><span class="ping pg1">Buổi 2: Ngữ pháp và từ vựng</span></td>
                  </tr>                      
                  <tr>
                    <td>09/09/2019</td>
                    <td>18h - 20h</td>
                    <td class="center"><a class="btn" href="">Nội dung bài học</a></td>
                    <td class="center"><a class="btn" href="">Bài tập</a></td>
                    <td style="width:350px">
                      <a class="ping pg1" href="">Test1</a>
                      <a class="ping pg2" href="">Test2</a>
                      <a class="ping pg3" href="">Test3</a>
                      <a class="ping pg3" href="">Test4</a>
                    </td>
                  </tr> 
                  <tr class="txt_min gray">
                    <td colspan="5"><span class="ping pg1">Buổi 3: Ngữ pháp và từ vựng</span></td>
                  </tr>                      
                  <tr class="gray">
                    <td>09/09/2019</td>
                    <td>18h - 20h</td>
                    <td class="center"><a class="btn" href="">Nội dung bài học</a></td>
                    <td class="center"><a class="btn" href="">Bài tập</a></td>
                    <td style="width:350px">
                      <a class="ping pg1" href="">Test1</a>
                      <a class="ping pg2" href="">Test2</a>
                      <a class="ping pg3" href="">Test3</a>
                      <a class="ping pg3" href="">Test4</a>
                    </td>
                  </tr>
                  <tr class="txt_min">
                    <td colspan="5"><span class="ping pg4">Buổi 4: Ngữ pháp và từ vựng</span></td>
                  </tr>                      
                  <tr>
                    <td>09/09/2019</td>
                    <td>18h - 20h</td>
                    <td class="center"><a class="btn" href="">Nội dung bài học</a></td>
                    <td class="center"><a class="btn" href="">Bài tập</a></td>
                    <td style="width:350px">
                      <a class="ping pg1" href="">Test1</a>
                      <a class="ping pg2" href="">Test2</a>
                      <a class="ping pg3" href="">Test3</a>
                      <a class="ping pg3" href="">Test4</a>
                    </td>
                  </tr> 
                  <tr class="txt_min gray">
                    <td colspan="5"><span class="ping pg2">Buổi 5: Ngữ pháp và từ vựng</span></td>
                  </tr>                      
                  <tr class="gray">
                    <td>09/09/2019</td>
                    <td>18h - 20h</td>
                    <td class="center"><a class="btn" href="">Nội dung bài học</a></td>
                    <td class="center"><a class="btn" href="">Bài tập</a></td>
                    <td style="width:350px">
                      <a class="ping pg1" href="">Test1</a>
                      <a class="ping pg2" href="">Test2</a>
                      <a class="ping pg3" href="">Test3</a>
                      <a class="ping pg3" href="">Test4</a>
                    </td>
                  </tr>
                  <tr class="txt_min">
                    <td colspan="5"><span class="ping pg2">Buổi 6: Ngữ pháp và từ vựng</span></td>
                  </tr>                      
                  <tr>
                    <td>09/09/2019</td>
                    <td>18h - 20h</td>
                    <td class="center"><a class="btn" href="">Nội dung bài học</a></td>
                    <td class="center"><a class="btn" href="">Bài tập</a></td>
                    <td style="width:350px">
                      <a class="ping pg1" href="">Test1</a>
                      <a class="ping pg2" href="">Test2</a>
                      <a class="ping pg3" href="">Test3</a>
                      <a class="ping pg3" href="">Test4</a>
                    </td>
                  </tr> 
                  <tr class="txt_min gray">
                    <td colspan="5"><span class="ping pg2">Buổi 7: Ngữ pháp và từ vựng</span></td>
                  </tr>                      
                  <tr class="gray">
                    <td>09/09/2019</td>
                    <td>18h - 20h</td>
                    <td class="center"><a class="btn" href="">Nội dung bài học</a></td>
                    <td class="center"><a class="btn" href="">Bài tập</a></td>
                    <td style="width:350px">
                      <a class="ping pg1" href="">Test1</a>
                      <a class="ping pg2" href="">Test2</a>
                      <a class="ping pg3" href="">Test3</a>
                      <a class="ping pg3" href="">Test4</a>
                    </td>
                  </tr>                                                                                    
                  <tr class="txt_min">
                    <td colspan="5"><span class="ping pg2">Buổi 8: Ngữ pháp và từ vựng</span></td>
                  </tr>                      
                  <tr>
                    <td>09/09/2019</td>
                    <td>18h - 20h</td>
                    <td class="center"><a class="btn" href="">Nội dung bài học</a></td>
                    <td class="center"><a class="btn" href="">Bài tập</a></td>
                    <td style="width:350px">
                      <a class="ping pg1" href="">Test1</a>
                      <a class="ping pg2" href="">Test2</a>
                      <a class="ping pg3" href="">Test3</a>
                      <a class="ping pg3" href="">Test4</a>
                    </td>
                  </tr> 
                  <tr class="txt_min gray">
                    <td colspan="5"><span class="ping pg2">Buổi 9: Ngữ pháp và từ vựng</span></td>
                  </tr>                      
                  <tr class="gray">
                    <td>09/09/2019</td>
                    <td>18h - 20h</td>
                    <td class="center"><a class="btn" href="">Nội dung bài học</a></td>
                    <td class="center"><a class="btn" href="">Bài tập</a></td>
                    <td style="width:350px">
                      <a class="ping pg1" href="">Test1</a>
                      <a class="ping pg2" href="">Test2</a>
                      <a class="ping pg3" href="">Test3</a>
                      <a class="ping pg3" href="">Test4</a>
                    </td>
                  </tr> 
                  <tr class="txt_min">
                    <td colspan="5"><span class="ping pg2">Buổi 10: Ngữ pháp và từ vựng</span></td>
                  </tr>                      
                  <tr>
                    <td>09/09/2019</td>
                    <td>18h - 20h</td>
                    <td class="center"><a class="btn" href="">Nội dung bài học</a></td>
                    <td class="center"><a class="btn" href="">Bài tập</a></td>
                    <td style="width:350px">
                      <a class="ping pg1" href="">Test1</a>
                      <a class="ping pg2" href="">Test2</a>
                      <a class="ping pg3" href="">Test3</a>
                      <a class="ping pg3" href="">Test4</a>
                    </td>
                  </tr> 
                  <tr class="txt_min gray">
                    <td colspan="5"><span class="ping pg2">Buổi 11: Ngữ pháp và từ vựng</span></td>
                  </tr>                      
                  <tr class="gray">
                    <td>09/09/2019</td>
                    <td>18h - 20h</td>
                    <td class="center"><a class="btn" href="">Nội dung bài học</a></td>
                    <td class="center"><a class="btn" href="">Bài tập</a></td>
                    <td style="width:350px">
                      <a class="ping pg1" href="">Test1</a>
                      <a class="ping pg2" href="">Test2</a>
                      <a class="ping pg3" href="">Test3</a>
                      <a class="ping pg3" href="">Test4</a>
                    </td>
                  </tr>
                  <tr class="txt_min">
                    <td colspan="5"><span class="ping pg2">Buổi 12: Ngữ pháp và từ vựng</span></td>
                  </tr>                      
                  <tr>
                    <td>09/09/2019</td>
                    <td>18h - 20h</td>
                    <td class="center"><a class="btn" href="">Nội dung bài học</a></td>
                    <td class="center"><a class="btn" href="">Bài tập</a></td>
                    <td style="width:350px">
                      <a class="ping pg1" href="">Test1</a>
                      <a class="ping pg2" href="">Test2</a>
                      <a class="ping pg3" href="">Test3</a>
                      <a class="ping pg3" href="">Test4</a>
                    </td>
                  </tr> 
                  <tr class="txt_min gray">
                    <td colspan="5"><span class="ping pg2">Buổi 13: Ngữ pháp và từ vựng</span></td>
                  </tr>                      
                  <tr class="gray">
                    <td>09/09/2019</td>
                    <td>18h - 20h</td>
                    <td class="center"><a class="btn" href="">Nội dung bài học</a></td>
                    <td class="center"><a class="btn" href="">Bài tập</a></td>
                    <td style="width:350px">
                      <a class="ping pg1" href="">Test1</a>
                      <a class="ping pg2" href="">Test2</a>
                      <a class="ping pg3" href="">Test3</a>
                      <a class="ping pg3" href="">Test4</a>
                    </td>
                  </tr>
                  <tr class="txt_min">
                    <td colspan="5"><span class="ping pg2">Buổi 14: Ngữ pháp và từ vựng</span></td>
                  </tr>                      
                  <tr>
                    <td>09/09/2019</td>
                    <td>18h - 20h</td>
                    <td class="center"><a class="btn" href="">Nội dung bài học</a></td>
                    <td class="center"><a class="btn" href="">Bài tập</a></td>
                    <td style="width:350px">
                      <a class="ping pg1" href="">Test1</a>
                      <a class="ping pg2" href="">Test2</a>
                      <a class="ping pg3" href="">Test3</a>
                      <a class="ping pg3" href="">Test4</a>
                    </td>
                  </tr> 
                  <tr class="txt_min gray">
                    <td colspan="5"><span class="ping pg2">Buổi 15: Ngữ pháp và từ vựng</span></td>
                  </tr>                      
                  <tr class="gray">
                    <td>09/09/2019</td>
                    <td>18h - 20h</td>
                    <td class="center"><a class="btn" href="">Nội dung bài học</a></td>
                    <td class="center"><a class="btn" href="">Bài tập</a></td>
                    <td style="width:350px">
                      <a class="ping pg1" href="">Test1</a>
                      <a class="ping pg2" href="">Test2</a>
                      <a class="ping pg3" href="">Test3</a>
                      <a class="ping pg3" href="">Test4</a>
                    </td>
                  </tr> 
                  <tr class="txt_min">
                    <td colspan="5"><span class="ping pg2">Buổi 16: Ngữ pháp và từ vựng</span></td>
                  </tr>                      
                  <tr>
                    <td>09/09/2019</td>
                    <td>18h - 20h</td>
                    <td class="center"><a class="btn" href="">Nội dung bài học</a></td>
                    <td class="center"><a class="btn" href="">Bài tập</a></td>
                    <td style="width:350px">
                      <a class="ping pg1" href="">Test1</a>
                      <a class="ping pg2" href="">Test2</a>
                      <a class="ping pg3" href="">Test3</a>
                      <a class="ping pg3" href="">Test4</a>
                    </td>
                  </tr> 
                  <tr class="txt_min gray">
                    <td colspan="5"><span class="ping pg2">Buổi 17: Ngữ pháp và từ vựng</span></td>
                  </tr>                      
                  <tr class="gray">
                    <td>09/09/2019</td>
                    <td>18h - 20h</td>
                    <td class="center"><a class="btn" href="">Nội dung bài học</a></td>
                    <td class="center"><a class="btn" href="">Bài tập</a></td>
                    <td style="width:350px">
                      <a class="ping pg1" href="">Test1</a>
                      <a class="ping pg2" href="">Test2</a>
                      <a class="ping pg3" href="">Test3</a>
                      <a class="ping pg3" href="">Test4</a>
                    </td>
                  </tr>
                  <tr class="txt_min">
                    <td colspan="5"><span class="ping pg2">Buổi 18: Ngữ pháp và từ vựng</span></td>
                  </tr>                      
                  <tr>
                    <td>09/09/2019</td>
                    <td>18h - 20h</td>
                    <td class="center"><a class="btn" href="">Nội dung bài học</a></td>
                    <td class="center"><a class="btn" href="">Bài tập</a></td>
                    <td style="width:350px">
                      <a class="ping pg1" href="">Test1</a>
                      <a class="ping pg2" href="">Test2</a>
                      <a class="ping pg3" href="">Test3</a>
                      <a class="ping pg3" href="">Test4</a>
                    </td>
                  </tr> 
                  <tr class="txt_min gray">
                    <td colspan="5"><span class="ping pg2">Buổi 19: Ngữ pháp và từ vựng</span></td>
                  </tr>                      
                  <tr class="gray">
                    <td>09/09/2019</td>
                    <td>18h - 20h</td>
                    <td class="center"><a class="btn" href="">Nội dung bài học</a></td>
                    <td class="center"><a class="btn" href="">Bài tập</a></td>
                    <td style="width:350px">
                      <a class="ping pg1" href="">Test1</a>
                      <a class="ping pg2" href="">Test2</a>
                      <a class="ping pg3" href="">Test3</a>
                      <a class="ping pg3" href="">Test4</a>
                    </td>
                  </tr>
                  <tr class="txt_min">
                    <td colspan="5"><span class="ping pg2">Buổi 20: Ngữ pháp và từ vựng</span></td>
                  </tr>                      
                  <tr>
                    <td>09/09/2019</td>
                    <td>18h - 20h</td>
                    <td class="center"><a class="btn" href="">Nội dung bài học</a></td>
                    <td class="center"><a class="btn" href="">Bài tập</a></td>
                    <td style="width:350px">
                      <a class="ping pg1" href="">Test1</a>
                      <a class="ping pg2" href="">Test2</a>
                      <a class="ping pg3" href="">Test3</a>
                      <a class="ping pg3" href="">Test4</a>
                    </td>
                  </tr> 
                  <tr class="txt_min gray">
                    <td colspan="5"><span class="ping pg2">Buổi 21: Ngữ pháp và từ vựng</span></td>
                  </tr>                      
                  <tr class="gray">
                    <td>09/09/2019</td>
                    <td>18h - 20h</td>
                    <td class="center"><a class="btn" href="">Nội dung bài học</a></td>
                    <td class="center"><a class="btn" href="">Bài tập</a></td>
                    <td style="width:350px">
                      <a class="ping pg1" href="">Test1</a>
                      <a class="ping pg2" href="">Test2</a>
                      <a class="ping pg3" href="">Test3</a>
                      <a class="ping pg3" href="">Test4</a>
                    </td>
                  </tr>
                  <tr class="txt_min">
                    <td colspan="5"><span class="ping pg2">Buổi 22: Ngữ pháp và từ vựng</span></td>
                  </tr>                      
                  <tr>
                    <td>09/09/2019</td>
                    <td>18h - 20h</td>
                    <td class="center"><a class="btn" href="">Nội dung bài học</a></td>
                    <td class="center"><a class="btn" href="">Bài tập</a></td>
                    <td style="width:350px">
                      <a class="ping pg1" href="">Test1</a>
                      <a class="ping pg2" href="">Test2</a>
                      <a class="ping pg3" href="">Test3</a>
                      <a class="ping pg3" href="">Test4</a>
                    </td>
                  </tr> 
                </tbody>
              </table>
            </div> 
            <!-- END BẢNG LỊCH HỌC -->  
            <div class="warp_bg document_ielts mb20">
              <?php echo $this->load->get_block('left_content'); ?>
            </div>
        </div>
        <div id="sidebar_right" class="col-md-4 col-sm-4 col-xs-4 col-tn-12 mb20">
            <div class="category">
                <a href=""><img width="100%" src="<?php echo $this->config->item('img')?>/graphics/dk-class.png" alt="this"></a>
            </div>
            
            <?php echo $this->load->get_block('user_right'); ?>                          
        </div>
    </div>        
</section>

<a class="pin-help open-popup-link" href="#help-popup"><img src="<?php echo $this->config->item('img')?>/icons/help.png"></a>  
<a class="pin-comment open-popup-link" href="javascript:;"><img src="<?php echo $this->config->item('img')?>/icons/comment.png"></a>  

<!-- SHOW HƯỚNG DẪN -->
<div id="help-popup" class="white-popup mfp-hide">
  <div class="content_poup width_common">
      <label class="title">Hướng dẫn học</label>
      <div class="scrollbar-inner">
        <p>Sử dụng Ulike trên điện thoại là một trong những cách đơn giản để giúp bạn có được những tấm hình “sống ảo” cực kỳ chất lượng. Bạn có thể dùng ứng dụng này trên cả thiết bị Android và iPhone. Hãy cùng tìm hiểu cách sử dụng ứng dụng này nhé!</p>
        <p>Bên cạnh những ứng dụng làm đẹp đang phổ biến hiện nay như Beauty Plus, B612…. thì hiện nay chúng ta có thêm một lựa chọn khác mang tên Ulike. Và cách tải và sử dụng Ulike trên điện thoại sẽ giúp bạn có thể sở hữu ứng dụng làm đẹp đang trở thành một điều không thể thiếu của mỗi người yêu thích chụp ảnh trên điện thoại.</p>
        <p>Để giúp bạn có thể tìm hiểu rõ ràng hơn về cách sử dụng cũng như cách tải Ulike trên điện thoại trong bài viết này chúng tôi sẽ sử dụng thiết bị Android để cùng các bạn thực hiện điều này và các bước thực hiện trên iPhone cũng khá tương tự.</p>
        <p><strong>Cách tải Ulike trên điện thoại</strong></p>
        <p>Bước 1 : Các bạn truy cập vào ứng dụng chợ ứng dụng trên điện thoại của mình là Google Play Store (hoặc CH Play) trên Android và App Store trên iPhone. Và sau đó chúng ta sẽ tìm kiếm ứng dụng Ulike.</p>
        <p>Bước 2 : Tại đây chúng ta sẽ nhập từ khoá “Ulike” và lựa chọn vào biểu tượng ứng dụng tương ứng. Tại đây để tải Ulike về điện thoại chúng ta sẽ chỉ việc ấn chọn mục Cài đặt .</p>
        <p>Bước 3 : Lúc này, hệ thống sẽ ngay lập tức tiến hành tải Ulike trên điện thoại cũng như cài đặt ứng dụng này hoàn toàn tự động. Khi quá trình này kết thúc, chúng ta sẽ có thể ấn chọn Mở để khởi động ứng dụng này ngay lập tức.</p>
        <p>Và như vậy, chúng ta đã hoàn thành việc tìm kiểm về cách tải và cài đặt Ulike trên thiết bị của mình rồi đấy. Cách sử dụng Ulike trên điện thoại</p>
        <p>Để có thể nắm bắt các bước sử dụng Ulike để có một bức ảnh đẹp hơn trên điện thoại như sau:</p>
        <p>Bước 1 : Các bạn khởi động ứng dụng Ulike sau khi đã tải và cài đặt thành công ứng dụng này.</p>
        <p>Bước 2 : Tại lần đầu truy cập sử dụng ứng dụng này, chúng ta sẽ thực hiện việc xác nhận các quyền truy cập vào hệ thống của Ulike như Camera, bộ nhớ…. bằng cách ấn chọn Cho phép , sau đó chúng ta sẽ được chuyển tới giao diện chính của ứng dụng này như hình dưới.</p>
        <p>Sử dụng Ulike trên điện thoại là một trong những cách đơn giản để giúp bạn có được những tấm hình “sống ảo” cực kỳ chất lượng. Bạn có thể dùng ứng dụng này trên cả thiết bị Android và iPhone. Hãy cùng tìm hiểu cách sử dụng ứng dụng này nhé!</p>
        <p>Bên cạnh những ứng dụng làm đẹp đang phổ biến hiện nay như Beauty Plus, B612…. thì hiện nay chúng ta có thêm một lựa chọn khác mang tên Ulike. Và cách tải và sử dụng Ulike trên điện thoại sẽ giúp bạn có thể sở hữu ứng dụng làm đẹp đang trở thành một điều không thể thiếu của mỗi người yêu thích chụp ảnh trên điện thoại.</p>
        <p>Để giúp bạn có thể tìm hiểu rõ ràng hơn về cách sử dụng cũng như cách tải Ulike trên điện thoại trong bài viết này chúng tôi sẽ sử dụng thiết bị Android để cùng các bạn thực hiện điều này và các bước thực hiện trên iPhone cũng khá tương tự.</p>
      </div>  
  </div>    
</div>    

<script>
      $(document).ready(function () {
        jQuery('.scrollbar-inner').scrollbar();
        $('.open-popup-link').magnificPopup({
          type:'inline',
          midClick: true,
          mainClass: 'mfp-with-zoom',
          fixedContentPos: false,
          fixedBgPos: true,
          overflowY: 'auto',
          closeBtnInside: true,
          preloader: false,
          removalDelay: 300,
        });

        /**Scroll to sticky**/
        $(window).scroll(function() {
            if($(window).scrollTop() >= 150)
            {
                $('.page-hv #main_menu_pc').addClass('show');
            }
            else
            {
                $('.page-hv #main_menu_pc').removeClass('show');
            }
        });

      });
    </script>    