<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Setting_model extends CI_Model {
	function __construct()
    {
        parent::__construct();
    }
    public function detail(){
    	$query = $this->db->get('setting');
    	return $query->row_array();
    }
    public function update($input){
        // update news 
        $input['setting'] = array_merge($input['setting'],array(
            'update_time' => time()
        ));
        $this->db->where('site_id',SITE_ID);
        $this->db->update('setting',$input['setting']);
        $countRow = $this->db->affected_rows();
        return $countRow;
    }
}