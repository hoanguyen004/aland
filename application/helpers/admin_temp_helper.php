<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 1: Check 1 - 0
 * 2: LIST CHECK BLANK
 */
function tmp_check_status($st, $type = 1){
    if ($type == 2){
        $st = ($st == '') ? 0 : 1;
    }
    if ($st < 1){
        $ext = '<span class="status"><i class="fa fa-times" aria-hidden="true"></i></span>';
	}
	else{
        $ext = '<span class="status"><i class="fa fa-check" aria-hidden="true"></i></span>';
	}
    return $ext;
}
function tmp_qfull_type($type){
    switch ($type) {
        case 1:
            return 'Full';
            break;
        case 2:
            return 'Mini';
            break;
        case 3:
            return 'Skill';
            break;
        default:
            break;
    }
}