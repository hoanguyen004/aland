-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 04, 2019 at 01:15 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aland`
--

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `test_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `images` varchar(255) DEFAULT NULL,
  `publish_time` int(11) DEFAULT NULL,
  `publish` tinyint(1) UNSIGNED DEFAULT NULL,
  `original_cate` int(10) UNSIGNED DEFAULT '0',
  `share_url` varchar(255) DEFAULT NULL,
  `test_time` int(11) UNSIGNED DEFAULT '0',
  `total_hit` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` int(10) DEFAULT NULL,
  `create_time` int(10) UNSIGNED DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `total_users` int(255) UNSIGNED DEFAULT '0',
  `isclass` tinyint(1) UNSIGNED DEFAULT '0',
  `type` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`test_id`, `title`, `description`, `images`, `publish_time`, `publish`, `original_cate`, `share_url`, `test_time`, `total_hit`, `user_id`, `create_time`, `update_time`, `total_users`, `isclass`, `type`) VALUES
(1, 'Practice Test 1', '', '', 1549815797, 1, 2, '/test/practice-test-1-1.html', 120, 0, 48, 1549815816, 1549815816, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `test_logs`
--

CREATE TABLE `test_logs` (
  `logs_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED DEFAULT NULL,
  `question_list` text,
  `score` int(11) DEFAULT NULL,
  `answer_list` text,
  `test_id` int(10) UNSIGNED DEFAULT NULL,
  `start_time` int(10) UNSIGNED DEFAULT NULL COMMENT 'thoi gian bat dau',
  `end_time` int(10) UNSIGNED DEFAULT NULL COMMENT 'thoi gian nop  bai',
  `score_detail` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `test_question`
--

CREATE TABLE `test_question` (
  `question_id` int(10) UNSIGNED NOT NULL,
  `title` text NOT NULL,
  `images` varchar(255) DEFAULT NULL,
  `detail` text,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `sound` varchar(255) DEFAULT NULL,
  `publish` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `test_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `type` int(10) UNSIGNED DEFAULT '0',
  `ordering` int(11) DEFAULT '0',
  `create_time` int(11) DEFAULT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `test_question`
--

INSERT INTO `test_question` (`question_id`, `title`, `images`, `detail`, `user_id`, `sound`, `publish`, `test_id`, `type`, `ordering`, `create_time`, `parent_id`) VALUES
(1, 'READING PASSAGE 1', '', '<div class=\"passage-description\">\r\n<p><em>You should spend about 20 minutes on <strong>Questions 1-13</strong>, which are based on Reading Passage 1 below.</em></p>\r\n</div>\r\n\r\n<div class=\"section-image\"><img height=\"auto\" src=\"https://ieltsonlinetests.com/sites/default/files/2018-11/The Innovation of Grocery Stores.jpg\"></div>\r\n\r\n<h2 class=\"subtitle\">The Innovation of Grocery Stores</h2>\r\n\r\n<div class=\"passage-content\">\r\n<p><strong>A </strong>At the very beginning of the 20th century, the American grocery stores offered comprehensive services: the customers would ask help from the people behind the counters (called clerks) for the items they liked, and then the clerks would wrap the items up. For the purpose of saving time, customers had to ask delivery boys or go in person to send the lists of what they intended to buy to the stores in advance and then went to pay for the goods later. Generally speaking, these grocery stores sold only one brand for each item. Such early chain stores as A&P stores, although containing full services, were very time-consuming and inefficient for the purchase.</p>\r\n\r\n<p><strong>B </strong>Bom in Virginia, Clarence Saunders left school at the age of 14 in 1895 to work first as a clerk in a grocery store. During his working in the store, he found that it was very inefficient for people to buy things there. Without the assistance of computers at that time, shopping was performed in a quite backward way. Having noticed that this inconvenient shopping mode could lead to tremendous consumption of time and money, Saunders, with great enthusiasm and innovation, proposed an unprecedented solution—let the consumers do self-service in the process of shopping—which might bring a thorough revolution to the whole industry.</p>\r\n\r\n<p><strong>C</strong> In 1902, Saunders moved to Memphis to put his perspective into practice, that is, to establish a grocery wholesale cooperative. In his newly designed grocery store, he divided the store into three different areas: ‘A front lobby’ served as an entrance, an exit, as well as the checkouts at the front. ‘A sales department’ was deliberately designed to allow customers to wander around the aisle and select their needed groceries. In this way, the clerks would not do the unnecessary work but arrange more delicate aisle and shelves to display the goods and enable the customers to browse through all the items. In the gallery above the sales department, supervisors can monitor the customers without disturbing them. ‘Stockroom’, where large fridges were placed to maintain fresh products, is another section of his grocery store only for the staff to enter. Also, this new shopping design and layout could accommodate more customers to go shopping simultaneously and even lead to some unimaginable phenomena: impulse buying and later supermarket.</p>\r\n\r\n<p><strong>D</strong> On September 6, 1916, Saunders performed the self-service revolution in the USA by opening the first Piggly Wiggly featured by the turnstile at the entrance store at 79 Jefferson Street in Memphis, Tennessee. Quite distinct from those in other grocery stores, customers in Piggly Wiggly chose the goods on the shelves and paid the items all by themselves. Inside the Piggly Wiggly, shoppers were not at the mercy of staff. They were free to roam the store, check out the products and get what they needed by their own hands. There, the items were clearly priced, and no one forced customers to buy the things they did not need. As a matter of fact, the biggest benefit that the Piggly Wiggly brought to customers was the money-saving effect. Self-service was optimistic for the improvement. ‘It is good for both the consumer and retailer because it cuts costs,’ noted George T. Haley, a professor at the University of New Haven and director of the Centre for International Industry Competitiveness, ‘if you look at the way in which grocery stores (previous to Piggly Wiggly and Alpha Beta) were operated, what you can find is that there are a great number of workers involved, and labour is a major expense.’ Fortunately, the chain stores such as Piggly Wiggly cut the fat.</p>\r\n\r\n<p><strong>E </strong>Piggly Wiggly and this kind of self-service stores soared at that time. In the first year, Saunders opened nine branches in Memphis. Meanwhile, Saunders immediately applied a patent for the self-service concept and began franchising Piggly Wiggly stores. Thanks to the employment of self-service and franchising, the number of Piggly Wiggly had increased to nearly 1,300 by 1923. Piggly Wiggly sold $100 million (worth $1.3 billion today) in groceries, which made it the third-biggest grocery retailer in the nation. After that, this chain store experienced company listing on the New York Stock Exchange, with the stocks doubling from late 1922 to March 1923. Saunders contributed significantly to the perfect design and layout of grocery stores. In order to keep the flow rate smooth, Saunders even invented the turnstile to replace the common entrance mode.</p>\r\n\r\n<p><strong>F</strong> Clarence Saunders died in 1953, leaving abundant legacies mainly symbolised by Piggly Wiggly, the pattern of which spread extensively and lasted permanently.</p>\r\n</div>\r\n', 48, '', 1, 1, 1, 1, 1551203116, 0),
(13, 'Questions 7-10', '', '<p><em>Answer the questions.</em></p>\r\n\r\n<p><em>Choose <span style=\"color:#ff0000;\"><strong>NO MORE THAN TWO WORDS</strong></span> from the passage for each answer.</em></p>\r\n', 48, '', 1, 1, 1, 2, 1551582183, 0),
(14, 'Questions 7-10', '', '<p><em>Answer the questions.</em></p>\r\n\r\n<p><em>Choose <span style=\"color:#ff0000;\"><strong>NO MORE THAN TWO WORDS</strong></span> from the passage for each answer.</em></p>\r\n', 48, '', 1, 1, 100, 3, 1551582339, 1),
(15, 'Questions 29-32', '', '<p><em>Complete the diagram.</em></p>\r\n\r\n<p><em>Choose <span style=\"color:#ff0000;\"><strong>NO MORE THAN TWO WORDS</strong></span> from the passage for each answer.</em></p>\r\n', 48, '', 1, 1, 101, 2, 1551600769, 1),
(16, 'Questions 1-3', '', '<p><em>Complete the diagram.</em></p>\r\n\r\n<p><em>Choose <span style=\"color:#ff0000;\"><strong>NO MORE THAN TWO WORDS</strong></span> from the passage for each answer.</em></p>\r\n', 48, '', 1, 1, 102, 5, 1551601809, 1),
(17, 'Reading Passage 2', '', '<p class=\"text-align-justify\">When movies made in one language are shown to speakers of another, the two methods of resolving the language barrier are subtitling and dubbing. Subtitling is the written translation of the words, usually appearing discreetly at the bottom of the screen, while dubbing is the recording of voices in the target language.</p>\r\n\r\n<p class=\"text-align-justify\">Dubbing, although seemingly more accessible to movie watches, comes with many disadvantages. For a start, it is expensive, hence it needs a large audience to justify the cost, yet even big films carry no guarantee of such commercial success. In addition, the dubbed voices may seem detached or inappropriate to the characters, or otherwise, the absurdity of having an undereducated American ranffian saying. ‘Je voudrais déclarer un vol’ becomes too much, affecting appreciation of the film. Finally, films and TV programs now have an increasingly rapid turnover rate, and subtitling is faster and more practical in such situations.</p>\r\n\r\n<p class=\"text-align-justify\">However, one should not assume subtitling is easier than dubbing. Subtitling requires careful strategies, and here I will outline some of them. In order to do this, a sample movie is needed, and the one examined here is an Italian movie subtitled into English. Comprehension of subtitles will always be affected by lack of familiarity with the values, beliefs, and interactive differences between the host and viewing cultures. The subtitlers need to be aware of this in order to translate true meaning. Thus, before beginning any work, a brief ‘cultural audit’ is absolutely necessary, involving a comparison of the two cultures in relation to the storyline of the movie.</p>\r\n\r\n<p class=\"text-align-justify\">The movie is set in the late 1960s, at a time when the wealth and materialism of American society was very high, contrasting the relative poverty of Italian village life. The plot tells the story of a poor couple who dream of winning large sums of money by gambling in a card game against a wealthy elderly American woman, who occasionally visits Italy just for that purpose. The final thematic assertion that there are more important factors than money reflects the warmth and solidarity of the Italian village in the face of adversity. Although these themes are universal, one could speculate that a Western audience might not like or identify with them as much, give the increasing urbanisation and materialism of their own society.</p>\r\n\r\n<p class=\"text-align-justify\">The most immediate translation issue relates to the movie’s title, ‘Lo Scopone Scientifico’, translates as ‘Scientific Scopone’, whereas the English title is, ‘The Scientific Card Player’. ‘Scopone’ is the name of a traditional Italian card game of great antiquity. Obviously, the translators could not use this name, obscure to the Westren viewers, but they insert a blander and inappropriate term. An even clearer subtitling lapse is that the betting is always done using, apparently, ludicrously high figures. Subtitles such as, ‘Let’s start with a million’ regularly jump out. This is a literal translation of the figures (in Italian lira), yet it is the dollar with which the English-speaking audience would associate. The result is an apparent lack of plausibility, changing the comedic nature of the film.</p>\r\n\r\n<p class=\"text-align-justify\">With respect to the specific subtitling used, there are five. Let us begin with the subtitle, ‘The old bag’s here.’ This is idiomatic in English, being an insulting term for an elderly woman. However, it is a simple expression comprising only two words, one of which is literally intended (‘old’). I would speculate that the same idiom occurs in Italian (that is, the direct translation of ‘old’ and ‘bag’ in Italian carries the same idiomatic meaning). This is the strategy of Transfer, where the full expression without time or space consideration is given. Otherwise, there could well be a closely aligned idiom, in which case the strategy would be <em>Imitation</em>, where there are similar lexical elements between both languages.</p>\r\n\r\n<p class=\"text-align-justify\">Continuing with idioms, we read, ‘Catches win matches’. This derives from certain ball games, such as cricket, where catching the ball after it is struck by the batsman contributes towards winning the game. There are no such sporting cultures in Italy followed. Thus, one can be certain that other words were used in the original Italian, but that these have a similar pragmatic effect (in meaning and idiomatic nature). The strategy used is thus <em>Paraphrasing</em>, where different expressions specific to the source language (Italian) and target language (English) are required.</p>\r\n', 48, '', 1, 1, 1, 3, 1551602309, 0),
(18, 'Questions 18-22', '', '<p><em>Match the translation example with its associated fact.</em></p>\r\n\r\n<p><em>Write the correct letter, <strong>A-D</strong>, next to the questions.</em></p>\r\n', 48, '', 1, 1, 100, 1, 1551602460, 1),
(19, 'Questions 29-32', '', '<h3><em>Choose the correct letter, <strong>A, B, C, </strong>or<strong> D.</strong></em></h3>\r\n', 48, '', 1, 1, 103, 5, 1551610295, 1),
(20, 'Questions 1-3', '', '', 48, '', 1, 1, 104, 1, 1551612307, 1),
(21, 'Section 1 : Questions 1-10', NULL, NULL, 48, 'quang_tao_cai_boong__huynh_james__pjnbo_128kbps_mp3.mp3', 1, 1, 2, 1, 1551613108, 0);

-- --------------------------------------------------------

--
-- Table structure for table `test_question_answer`
--

CREATE TABLE `test_question_answer` (
  `answer_id` int(10) UNSIGNED NOT NULL,
  `content` text,
  `question_id` int(10) UNSIGNED NOT NULL,
  `ordering` tinyint(1) UNSIGNED NOT NULL,
  `params` text,
  `number_question` tinyint(2) UNSIGNED DEFAULT NULL,
  `options` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `test_question_answer`
--

INSERT INTO `test_question_answer` (`answer_id`, `content`, `question_id`, `ordering`, `params`, `number_question`, `options`) VALUES
(10, NULL, 14, 0, '[\"When glasses were invented, most people could read. \",\"When glasses were invented, most people could read. \",\"When glasses were invented, most people could read. \"]', 3, NULL),
(12, '<p><img alt=\"\" src=\"http://aland.hanoicdc.com/uploads/images/userfiles/1.jpg\" style=\"width: 600px; height: 400px;\"></p>\r\n', 16, 0, '[\"What is needed when viewing through bi-focal glasses?\",\"What can cause condensation on glasses?\",\"What can cause condensation on glasses?\"]', 3, '[]'),
(13, NULL, 18, 0, '[\"The English title is a subtitling lapse. \",\"Transfer and imitation are interesting strategies.\",\"Transfer and imitation are interesting strategies.\"]', 3, '[]'),
(14, NULL, 19, 0, '[\"Scientists disliked Wegener\\u2019s idea because he\"]', 1, '[\"was German.\",\"made simple observations.\",\"was a meteorologist.\",\"made too many suggestions.\"]');

-- --------------------------------------------------------

--
-- Table structure for table `test_question_answer_result`
--

CREATE TABLE `test_question_answer_result` (
  `answer_detail_id` int(10) UNSIGNED NOT NULL,
  `answer_id` int(10) UNSIGNED DEFAULT NULL,
  `answer` varchar(255) DEFAULT NULL,
  `ordering` tinyint(2) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `test_question_answer_result`
--

INSERT INTO `test_question_answer_result` (`answer_detail_id`, `answer_id`, `answer`, `ordering`) VALUES
(14, 10, 'false', 1),
(15, 10, 'true', 2),
(16, 10, 'notgiven', 3),
(49, 12, 'abc', 1),
(50, 12, 'cde', 2),
(51, 12, 'efg', 3),
(52, 13, 'false', 1),
(53, 13, 'true', 2),
(54, 13, 'notgiven', 3),
(56, 14, 'B', 1);

-- --------------------------------------------------------

--
-- Table structure for table `test_to_class`
--

CREATE TABLE `test_to_class` (
  `test_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `class_id` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`test_id`),
  ADD KEY `fk_test_1` (`original_cate`);

--
-- Indexes for table `test_logs`
--
ALTER TABLE `test_logs`
  ADD PRIMARY KEY (`logs_id`) USING BTREE,
  ADD KEY `fk_test_logs_1` (`user_id`) USING BTREE,
  ADD KEY `fk_test_logs_2` (`test_id`) USING BTREE;

--
-- Indexes for table `test_question`
--
ALTER TABLE `test_question`
  ADD PRIMARY KEY (`question_id`),
  ADD KEY `fk_test_question_1` (`test_id`);

--
-- Indexes for table `test_question_answer`
--
ALTER TABLE `test_question_answer`
  ADD PRIMARY KEY (`answer_id`),
  ADD KEY `idx_answer_quesID` (`question_id`);

--
-- Indexes for table `test_question_answer_result`
--
ALTER TABLE `test_question_answer_result`
  ADD PRIMARY KEY (`answer_detail_id`) USING BTREE,
  ADD KEY `fk_test_question_answer_detail_1` (`answer_id`);

--
-- Indexes for table `test_to_class`
--
ALTER TABLE `test_to_class`
  ADD PRIMARY KEY (`test_id`,`class_id`),
  ADD KEY `fk_test_to_class_2` (`class_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `test_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `test_logs`
--
ALTER TABLE `test_logs`
  MODIFY `logs_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `test_question`
--
ALTER TABLE `test_question`
  MODIFY `question_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `test_question_answer`
--
ALTER TABLE `test_question_answer`
  MODIFY `answer_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `test_question_answer_result`
--
ALTER TABLE `test_question_answer_result`
  MODIFY `answer_detail_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `test`
--
ALTER TABLE `test`
  ADD CONSTRAINT `test_ibfk_1` FOREIGN KEY (`original_cate`) REFERENCES `category` (`cate_id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `test_logs`
--
ALTER TABLE `test_logs`
  ADD CONSTRAINT `test_logs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `test_logs_ibfk_2` FOREIGN KEY (`test_id`) REFERENCES `test` (`test_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `test_question`
--
ALTER TABLE `test_question`
  ADD CONSTRAINT `test_question_ibfk_1` FOREIGN KEY (`test_id`) REFERENCES `test` (`test_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `test_question_answer`
--
ALTER TABLE `test_question_answer`
  ADD CONSTRAINT `test_question_answer_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `test_question` (`question_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `test_question_answer_result`
--
ALTER TABLE `test_question_answer_result`
  ADD CONSTRAINT `fk_test_question_answer_detail_1` FOREIGN KEY (`answer_id`) REFERENCES `test_question_answer` (`answer_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `test_to_class`
--
ALTER TABLE `test_to_class`
  ADD CONSTRAINT `test_to_class_ibfk_1` FOREIGN KEY (`test_id`) REFERENCES `test` (`test_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `test_to_class_ibfk_2` FOREIGN KEY (`class_id`) REFERENCES `class` (`class_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
